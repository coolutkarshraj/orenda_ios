//
//  CustomAlertViewController.swift
//  GetBlushh
//
//  Created by Chaithu on 02/04/18.
//  Copyright © 2018 Arvind Mehta. All rights reserved.
//

import UIKit

protocol CustomAlertViewDelegate: class {
    func okButtonTapped()
    func cancelButtonTapped()
}
class CustomAlertViewController: UIViewController {

    
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var btYes: UIButton!
    @IBOutlet weak var btNo: UIButton!
    @IBOutlet weak var lable: UILabel!
     var delegate: CustomAlertViewDelegate?
    override func viewDidLoad()
    {
        super.viewDidLoad()

        if !UIAccessibility.isReduceTransparencyEnabled {
            self.view.backgroundColor = .clear
            let visualEffectView   = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
            visualEffectView.alpha = 0.8
            visualEffectView.frame = self.view.bounds
            visualEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
           
            self.view.insertSubview(visualEffectView, at: 0)
        } else {
            view.backgroundColor = .black
        }
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
      
       // animateView()
    }
    
 
    func animateView() {
        alertView.alpha = 0;
        self.alertView.frame.origin.y = self.alertView.frame.origin.y + 50
        UIView.animate(withDuration: 0.4, animations: { () -> Void in
            self.alertView.alpha = 1.0;
            self.alertView.frame.origin.y = self.alertView.frame.origin.y - 50
        })
    }
    
    @IBAction func onTapCancelButton(_ sender: Any) {
       
        delegate?.cancelButtonTapped()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onTapOkButton(_ sender: Any) {
        
        delegate?.okButtonTapped()
        self.dismiss(animated: true, completion: nil)
    }
    
    
}
