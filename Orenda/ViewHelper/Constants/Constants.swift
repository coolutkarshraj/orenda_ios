//
//  Constants.swift
//  GetBlushh
//
//  Created by Arvind Mehta on 13/12/17.
//  Copyright © 2017 Arvind Mehta. All rights reserved.
//

import Foundation
struct Constants {
  // ---  Identifires
    static let LOGIN:String = "Login"
    static let REGISTER:String = "SignUp"
    static let OTP_CONTROLLER:String = "OTP_controller"
    static let REGISTER_PASSWORD:String = "Register_password"
    static let REGISTER_REFFERAL:String = "Register_referral"
    static let TABBAR_CONTROLLER  = "Dashbord_Tabbar_Controller"
    static let SWRevealViewController = "SWRevealViewController"
    static let WALLETVIEWCONTROLLER = "walletViewController"
    static let PREPAID_CONTROLLER = "prepaid_controller"
    static let OPERATOR_SELECT = "operator_listing"
    static let SEEPLAN_CONTROLLER = "seeplanViewController"
    static let DataCardPlanViewController = "DataCardPlanViewController"
    static let CIRCLE_CONTROLLER = "circleViewController"
    static let CONFIRM_RECHARGE = "confirmRechargeViewController"
    static let CONFIRM_RECHARGE_Without_Plan = "ConfirmRechargeWithoutPlanViewController"
    static let CONFIRM_RECHARGE_Payment = "PaymentMethodController"
    static let DATA_CARD_CONTRoLLER = "DataCardController"
    static let DTH_CARD_COntroller = "DthViewController"
    static let DTHPlanViewController = "DTHPlanViewController"
    
    
    
    // -- Param
    
    static let USER_TOKEN:String = "user_token"
    static let USER_MOBILE:String = "mobile_number"
    static let USER_OTP:String = "otp"
    static let USER_EMAIL:String = "email"
    static let USER_DEVICE_TYPE:String = "device_type"
    static let USER_DEVICE_INFO:String = "device_info"
    static let USER_DEVICE_ID:String = "device_id"
    static let USER_DEVICE_NOTIFICATION_TOKEN:String = "fcm_token"
    static let USER_FULL_NAME:String = "full_name"
    static let USER_LOGIN_STATUS:String = "login_status"
    static let USER_ID:String = "user_id"
    static let USER_BALANCE:String = "balance"
    
    
    //Meesage View messageType
    static let INFO:Int = 0
    static let SUCCESS:Int = 1
    static let WARNING:Int = 2
    static let ERROR:Int = 3
    static let DURATION_LONG:Double = 4.0
    static let DURATION_MIDIUM:Double = 2.0
    static let DURATION_SHORT:Double = 1.0
    
    //Events
    static let Operator_Event = "selectOperatorEvent"
    static let Close_OperatorListing = "close_operatorListing"
     static let Circle_Event = "selectcircleEvent"
    static let closeCircleListing = "closeCircleEvent"
     static let selectPlan = "selectPlan"
    static let closePlanlisting = "closePlanlisting"
    static var screenType = ""
    static var  isViewPlanScreen = "0"
    static var tempIsPlanScreenView = ""
    static var tempAmount = ""
}
