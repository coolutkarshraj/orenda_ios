//
//  MyTextField.swift
//  Mether
//
//  Created by Chaithu on 10/04/18.
//  Copyright © 2018 MetherTech. All rights reserved.
//

import UIKit

@IBDesignable class MyView: UIView {

    
    var colcoCode  = "000000"
    override func layoutSubviews()
    {
            super.layoutSubviews()
            
            updateCornerRadius()
        
    
      
     }

    @IBInspectable var cornerRadious: CGFloat = 0.0
        {
        didSet
        {
            updateCornerRadius()
        }
    }
    @IBInspectable var rounded: Bool = false
        {
        didSet
        {
            updateCornerRadius()
        }
    }
//    @IBInspectable var borderColor: String = "000000"
//        {
//        didSet
//        {
//            colcoCode = borderColor
//            updateCornerRadius()
//        }
//    }
    
    func updateCornerRadius()
    {
        if(cornerRadious == 0.0){
             layer.cornerRadius = rounded ? frame.size.height / 2 : 0
        }else{
            layer.cornerRadius = cornerRadious
        }
       
        layer.borderWidth = 1
        layer.borderColor = hexStringToUIColor(hex: colcoCode).cgColor
    }
}
