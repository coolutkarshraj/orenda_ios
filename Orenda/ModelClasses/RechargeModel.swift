//
//  RechargeModel.swift
//  Orenda
//
//  Created by Arvind Mehta on 02/12/18.
//  Copyright © 2018 Arvind Mehta. All rights reserved.
//

import Foundation
import SwiftyJSON
class RechargeModel{
    public var operatorName = ""
    public var opratorImage = ""
    public var circleName = ""
    public var operatorId = ""
    public var circleId  = ""
    public var isCricleActive = ""
    public var label = ""
    public var maxLimit = ""
    public var minLimit = ""
    public var operatorInnerModelArrayList = [OperatorInnerModel]()
    
    init(json:JSON) {
         operatorName = json["operator"].stringValue
         opratorImage = json["image_url"].stringValue
        if(json["name"].exists()){
            circleName = json["name"].stringValue
        }
        
         operatorId = json["id"].stringValue
        circleId = json["id"].stringValue
        if(json["operator_urls"].exists()){
            for item in json["operator_urls"].arrayValue{
                operatorInnerModelArrayList.append(OperatorInnerModel(json: item))
            }
        }
       
        
    }
    
    class OperatorInnerModel{
        public var id = ""
        public var status  = ""
        public var type = ""
        init(json:JSON) {
            id = json["id"].stringValue
            status = json["status"].stringValue
            type = json["type"].stringValue
        }
    }
}
