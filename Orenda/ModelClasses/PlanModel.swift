//
//  PlanModel.swift
//  Orenda
//
//  Created by Arvind Mehta on 02/12/18.
//  Copyright © 2018 Arvind Mehta. All rights reserved.
//

import Foundation
import Gloss

class PlanModel : JSONDecodable{
    
    let id:String?
    let operator_id:String?
    let circle_id:String?
    let recharge_amount:String?
    let recharge_talktime:String?
    let recharge_validity:String?
    let recharge_short_desc:String?
    let recharge_long_desc:String?
    let recharge_type:String?
    let updated_at:String?
    
    required init?(json: JSON) {
        
        self.id = "id" <~~ json
         self.operator_id = "operator_id" <~~ json
        self.circle_id = "circle_id" <~~ json
        self.recharge_amount = "recharge_amount" <~~ json
        self.recharge_talktime = "recharge_talktime" <~~ json
        self.recharge_validity = "recharge_validity" <~~ json
        self.recharge_short_desc = "recharge_short_desc" <~~ json
        self.recharge_long_desc = "recharge_long_desc" <~~ json
        self.recharge_type = "recharge_type" <~~ json
        self.updated_at = "updated_at" <~~ json
    }
    
    
}
