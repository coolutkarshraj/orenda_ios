//
//  LoginViewController.swift
//  Orenda
//
//  Created by Arvind Mehta on 22/11/18.
//  Copyright © 2018 Arvind Mehta. All rights reserved.
//

import UIKit

class LoginViewController: BaseViewController ,UITextFieldDelegate{
    @IBOutlet weak var tf_password: UITextField!
    
    @IBOutlet weak var resetTextField: UITextField!
    @IBOutlet weak var restFulView: UIView!
    @IBOutlet weak var resetView: UIView!
    @IBOutlet weak var containerHeight: NSLayoutConstraint!
    @IBOutlet weak var passwordViewHeight: NSLayoutConstraint!
    @IBOutlet weak var tfPasswordHeight: NSLayoutConstraint!
    @IBOutlet weak var lb_forgotPassword: UILabel!
    @IBOutlet weak var lb_dontHaveAccount: UILabel!
    @IBOutlet weak var tf_emailPhone: UITextField!
    
    var plan = ""
    var talkTime = ""
    var validity = ""
    var opoperator = ""
    var mobile = ""
    var amount = ""
    var type = ""
    var operator_id = ""
    var rupeesSign = "INR"
    
    var isalreadyShowing = false
    override func viewDidLoad() {
        super.viewDidLoad()
        tf_emailPhone.delegate = self
        restFulView.isHidden = true
        hidePasswordView()
        resetView.roundCorners([.topLeft,.topRight], radius: 20)
        
        let tapdonthaveCode = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.tapDonthaveAccount))
        lb_dontHaveAccount.isUserInteractionEnabled = true
        lb_dontHaveAccount.addGestureRecognizer(tapdonthaveCode)
        
        let tapforgotPassword = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.tapForgotPassword))
        lb_forgotPassword.isUserInteractionEnabled = true
        lb_forgotPassword.addGestureRecognizer(tapforgotPassword)
        
       
    }
    
    //Action
    @objc func tapDonthaveAccount() {
       switchControllersToNew(identifier: Constants.REGISTER)
    }
    @objc func tapForgotPassword() {
       restFulView.isHidden = false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let fullString = textField.text ?? ""
        if(fullString.count>6){
            if(!isalreadyShowing){
                  showPasswordView()
            }
        }else{
            hidePasswordView()
        }
      
        return true
    }
    

    @IBAction func onContinueClick(_ sender: Any) {
        if (tf_emailPhone.text?.isBlank)! {
            showMessage(titleText: "Error", bodyText: "Please enter your email or mibile number", msgtype: Constants.ERROR, duration: Constants.DURATION_MIDIUM)
        }else if (tf_password.text?.isBlank)!{
             showMessage(titleText: "Error", bodyText: "Please enter your password", msgtype: Constants.ERROR, duration: Constants.DURATION_MIDIUM)
        }else{
            submitdata(mobile: tf_emailPhone.text!, password: tf_password.text!)
           
        }

    }
    
    func submitdata(mobile:String,password:String)  {
        authentication(view: self.view, email: mobile, password: password){response in
            Log(type: "LoginResponce", msg: response)
            if(response["error_code"].intValue == 400){
                showMessage(titleText: "Error", bodyText: response["message"].stringValue, msgtype: Constants.WARNING, duration: Constants.DURATION_MIDIUM)
            }else {
                 showMessage(titleText: "Success", bodyText: response["message"].stringValue, msgtype: Constants.SUCCESS, duration: Constants.DURATION_MIDIUM)
                
                if(Constants.isViewPlanScreen == "0"){
                    self.switchControllersToRechargeConfirmWithoutPlan(tfOfSisiter: self.amount,number: self.mobile,opId: self.operator_id,rechtype: self.type,circle: self.opoperator,selectedplan: self.plan,selectedValidity: self.talkTime,rechargeType: self.type)
                }else if(Constants.isViewPlanScreen == "1"){
                    self.switchControllersToRechargeConfirm(tfOfSisiter: self.amount,number: self.mobile,opId: self.self.operator_id,rechtype: self.type,circle: self.opoperator,selectedplan: self.plan,selectedValidity: self.talkTime,rechargeType: self.type)
                }else if(Constants.isViewPlanScreen == "2"){
                   self.switchControllersToNew(identifier: Constants.SWRevealViewController)

                }else{
                   self.switchControllersToNew(identifier: Constants.SWRevealViewController)
                }
           }
        }
    }
    
   
    
    func hidePasswordView() {
        isalreadyShowing = false
        containerHeight.constant = 150
        passwordViewHeight.constant = 0
        tfPasswordHeight.constant = 0
    }
    func showPasswordView() {
        isalreadyShowing = true
        containerHeight.constant = 220
        passwordViewHeight.constant = 60
        tfPasswordHeight.constant = 35
    }
    
    @IBAction func onresetSubmit(_ sender: Any) {
        if(!(resetTextField.text?.isBlank)!){
            resetSubmit(email: resetTextField.text!)
        }else{
            showMessage(titleText: "Error", bodyText: "Please enter your email", msgtype: Constants.ERROR, duration: Constants.DURATION_MIDIUM)
        }
    }
    @IBAction func onresetcancel(_ sender: Any) {
        restFulView.isHidden = true
    }
    
    func resetSubmit(email:String){
         restFulView.isHidden = true
        forgotPassword(view: self.view, email:email){ responce in
            Log(type: "Responce", msg: responce)
            
        }
    }
}
