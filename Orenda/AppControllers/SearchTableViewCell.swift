//
//  SearchTableViewCell.swift
//  ConnectAdlinks
//
//  Created by Arvind Mehta on 27/11/18.
//  Copyright © 2018 Pixel. All rights reserved.
//

import UIKit

class SearchTableViewCell: UITableViewCell {

    @IBOutlet weak var lbSearch: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
