//
//  PageTableViewCell.swift
//  Orenda
//
//  Created by Arvind Mehta on 23/11/18.
//  Copyright © 2018 Arvind Mehta. All rights reserved.
//

import UIKit
import FSPagerView

class PageTableViewCell: UITableViewCell,FSPagerViewDataSource,FSPagerViewDelegate {
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return images.count
    }
    
  
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        if(savedImages.count <= images.count){
            downloadImage(imageView: cell.imageView!, path: images[index]){image in
                self.savedImages.append(image)
            }
        }else{
            cell.imageView?.image = savedImages[index]
        }
       
       // cell.imageView?.image = UIImage(named: self.imageNames[index])
        self.pageControl.currentPage = index
        cell.imageView?.contentMode = .scaleAspectFill
        cell.imageView?.clipsToBounds = true
        return cell
    }
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        pagerView.deselectItem(at: index, animated: true)
    }
    
    // MARK:- FSPagerViewDelegate
    
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        self.pageControl.currentPage = targetIndex
    }
    
    fileprivate let transformerTypes: [FSPagerViewTransformerType] = [.crossFading,
                                                                      .zoomOut,
                                                                      .depth,
                                                                      .linear,
                                                                      .overlap,
                                                                      .ferrisWheel,
                                                                      .invertedFerrisWheel,
                                                                      .coverFlow,
                                                                      .cubic]
    
    var images = ["https://images.pexels.com/photos/34950/pexels-photo.jpg?auto=compress&cs=tinysrgb&h=350","https://cdn.pixabay.com/photo/2018/06/09/22/56/peacock-3465442__340.jpg","https://cdn.pixabay.com/photo/2018/02/09/21/46/rose-3142529__340.jpg","https://www.elastic.co/assets/bltada7771f270d08f6/enhanced-buzz-1492-1379411828-15.jpg"]
    
    var savedImages = [UIImage]()

    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var fsPagerView: FSPagerView!
    var titleArray = [String]()
    override func awakeFromNib() {
        super.awakeFromNib()
        fsPagerView.delegate = self
        fsPagerView.dataSource = self
//        let type = self.transformerTypes[2]
//        self.fsPagerView.transformer = FSPagerViewTransformer(type:type)
        
        self.fsPagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
        fsPagerView.automaticSlidingInterval = 3.0
        pageControl.currentPage = 0
        pageControl.numberOfPages = images.count
        
       
        
     
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
