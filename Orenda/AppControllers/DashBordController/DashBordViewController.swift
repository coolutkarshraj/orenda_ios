//
//  DashBordViewController.swift
//  Orenda
//
//  Created by Arvind Mehta on 22/11/18.
//  Copyright © 2018 Arvind Mehta. All rights reserved.
//

import UIKit

class DashBordViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource{
   
    
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cellIdentifier = "PageTableViewCell"
          let cellIdentifiercollection = "BillPaymentTableViewCell"
        
        if(indexPath.row == 0){
           let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! PageTableViewCell
            cell.fsPagerView.roundCorners([.topLeft, .topRight], radius: 20)
            


            return cell
       }else{
             let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifiercollection, for: indexPath) as! BillPaymentTableViewCell
            
           
            
            return cell
      }
        
    }
    
    @IBAction func LandLine(_ sender: UIButton) {
        switchControllersToNew(identifier: Constants.DATA_CARD_CONTRoLLER)
    }
    @IBAction func DthModule(_ sender: UIButton) {
        switchControllersToNew(identifier: Constants.DTH_CARD_COntroller)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.row {
        case 0:
            return 200
        case 1:
            return 230
        case 2:
            return 230
        default:return 230
            
        }
       
    }
    
  
    

   
    @IBOutlet weak var tableparallex: ParallaxTableView!
    @IBOutlet weak var btMenu: UIButton!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        Constants.isViewPlanScreen = "2"
        sideMenuSetUp()
        tableparallex.backgroundColor = .clear
        self.tableparallex.separatorStyle = .none
        //tableparallex.constructParallaxHeader()
        // Do any additional setup after loading the view.
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
         tableparallex.updateHeaderView()
    }
    
   
    

    func sideMenuSetUp()  {
        if self.revealViewController() != nil
        {
            self.btMenu .addTarget(revealViewController(), action:#selector(SWRevealViewController.revealToggle(_:)), for:.touchUpInside)
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.revealViewController().rearViewRevealWidth = 280
        }
    }
    
  
    @IBAction func onWalletclick(_ sender: Any) {
        switchControllersToNew(identifier: Constants.WALLETVIEWCONTROLLER)
    }
    
    @IBAction func onPrepaidClick(_ sender: Any) {
        Constants.screenType = "PrePaidRechargeScreen"
        switchControllersToNew(identifier: Constants.PREPAID_CONTROLLER)
    }
    
    @IBAction func onPostPaidclick(_ sender: Any) {
        Constants.screenType = "postPaidRechargeScreen"
        switchControllersToNew(identifier: Constants.PREPAID_CONTROLLER)
       
    }
    
    @IBAction func dataCard(_ sender: UIButton) {
        switchControllersToNew(identifier: Constants.DATA_CARD_CONTRoLLER)
    }
}

