//
//  DTHPlanViewController.swift
//  Orenda
//
//  Created by Pixel on 07/01/19.
//  Copyright © 2019 Arvind Mehta. All rights reserved.
//

import UIKit
import SwiftEventBus
import Parchment
class DTHPlanViewController: BaseViewController {
    
    var rechargeType = ""
    var selectedOperator = ""
    var selectedCircle = ""
    @IBOutlet weak var backMenu: UIImageView!
    @IBOutlet weak var container: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
       
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let firstViewController = storyboard.instantiateViewController(withIdentifier: "SeeDTHPLanViewController") as! SeeDTHPLanViewController
        let secondViewController = storyboard.instantiateViewController(withIdentifier: "SeeDTHPLanViewController") as! SeeDTHPLanViewController
        let thirdViewController = storyboard.instantiateViewController(withIdentifier: "SeeDTHPLanViewController") as! SeeDTHPLanViewController
        let fourthViewController = storyboard.instantiateViewController(withIdentifier: "SeeDTHPLanViewController") as! SeeDTHPLanViewController
      
        
        firstViewController.title = "MONTHLY PACK"
        secondViewController.title = "3 MONTHS PACK"
        thirdViewController.title = "6 MONTHS PACK"
        fourthViewController.title = "ANNUAL PACK"
       
        
        firstViewController.rechargeType = rechargeType
        firstViewController.selectedCircle = selectedCircle
        firstViewController.selectedOperator = selectedOperator
        
        secondViewController.rechargeType = rechargeType
        secondViewController.selectedCircle = selectedCircle
        secondViewController.selectedOperator = selectedOperator
        
        thirdViewController.rechargeType = rechargeType
        thirdViewController.selectedCircle = selectedCircle
        thirdViewController.selectedOperator = selectedOperator
        
        fourthViewController.rechargeType = rechargeType
        fourthViewController.selectedCircle = selectedCircle
        fourthViewController.selectedOperator = selectedOperator

        
        
        
        
        
        // Initialize a FixedPagingViewController and pass
        // in the view controllers.
        let pagingViewController = FixedPagingViewController(viewControllers: [
            firstViewController,
            secondViewController,
            thirdViewController,
            fourthViewController,
            ])
        
        // Make sure you add the PagingViewController as a child view
        // controller and contrain it to the edges of the view.
        addChild(pagingViewController)
        container.addSubview(pagingViewController.view)
        container.constrainToEdges(pagingViewController.view)
        pagingViewController.didMove(toParent: self)
        
        SwiftEventBus.onMainThread(self, name: Constants.closePlanlisting) { result in
            self.dismiss(animated: true, completion: nil)
        }
        
        
    }
    
    func gesterTouch(){
        let tapdonthaveCode = UITapGestureRecognizer(target: self, action: #selector(SeePlanViewController.tapbackPress))
        backMenu.isUserInteractionEnabled = true
        backMenu.addGestureRecognizer(tapdonthaveCode)
    }
    
    //Action
    @objc func tapbackPress() {
        Constants.isViewPlanScreen = "0"
        dismiss(animated: true, completion: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        SwiftEventBus.unregister(self)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
