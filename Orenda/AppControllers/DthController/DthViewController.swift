//
//  DthViewController.swift
//  Orenda
//
//  Created by Pixel on 07/01/19.
//  Copyright © 2019 Arvind Mehta. All rights reserved.
//

import UIKit
import SwiftEventBus
import Parchment

class DthViewController: BaseViewController{
    @IBOutlet weak var viewPlan: UIView!
    @IBOutlet weak var seeplan: UIButton!
  
    
    @IBOutlet weak var backMenu: UIImageView!
  
    @IBOutlet weak var selectOperator: UILabel!
    @IBOutlet weak var EnterMobileNumber: UITextField!
    @IBOutlet weak var EnterAmount: UITextField!
     var rechageType = "5"
     var type_ = "5"
    var selectedOperator = ""
    var selectedCircle = ""
    var plan = ""
    var enteredAmount = ""
    var talkTime = ""
    var validity = ""
    var operatorId = ""
    override func viewDidLoad() {
        Constants.screenType = "DTHController"
        super.viewDidLoad()
        gesterTouch()
        setUpEvent()
        Constants.isViewPlanScreen = "0"
    }
    @IBAction func continueToDthConfirm(_ sender: UIButton) {
        
        let ooperator = selectOperator.text
        let ooperatorn = selectOperator.text
        
        let tfOfSisiter = EnterAmount.text!
        let number  = EnterMobileNumber.text
        let opId = operatorId
        let rechtype = type_
        let circle =  "\(ooperatorn!),"
        let selectedplan = plan
        let selectedValidity = validity
        if( ((ooperator?.isEmpty)!) || number!.isEmpty || (tfOfSisiter.isEmpty) || rechtype.isEmpty){
            print(".....................emppty"  + ooperator! + "gggg" + number!+"hhhhh" + tfOfSisiter, "nnnnn" + rechtype)
        }else{
            if isLogin(){
                
                navigateScreen()
            }else{
                
                switchLoginControllersfromRechargeConfirm(tfOfSisiter: tfOfSisiter,number: number!,opId: opId,rechtype: rechtype,circle: ooperatorn!,selectedplan: selectedplan,selectedValidity: selectedValidity,rechargeType: type_)
            }
            
        }
    }
    
    func navigateScreen(){
        let ooperatorn = selectOperator.text
        
        let tfOfSisiter = EnterAmount.text!
        let number  = EnterMobileNumber.text!
        let opId = operatorId
        let rechtype = type_
        let circle =  "\(ooperatorn!),"
        let selectedplan = plan
        let selectedValidity = validity
        print(opId)
        if Constants.isViewPlanScreen == "0"{
            print(tfOfSisiter)
            switchControllersToRechargeConfirmWithoutPlan(tfOfSisiter: tfOfSisiter,number: number,opId: opId,rechtype: rechtype,circle: ooperatorn!,selectedplan: selectedplan,selectedValidity: selectedValidity,rechargeType: type_)
            
        }else{
            print("Without Plan")
            print(circle,selectedValidity)
            switchControllersToRechargeConfirm(tfOfSisiter: tfOfSisiter,number: number,opId: opId,rechtype: rechtype,circle: ooperatorn!,selectedplan: selectedplan,selectedValidity: selectedValidity,rechargeType: type_)
            
            
        }
    }
    
    func gesterTouch(){
        let tapdonthaveCode = UITapGestureRecognizer(target: self, action: #selector(PrepaidViewController.tapbackPress))
        backMenu.isUserInteractionEnabled = true
        backMenu.addGestureRecognizer(tapdonthaveCode)
    }
    
    //Action
    @objc func tapbackPress() {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func selectOperator(_ sender: UIButton) {
        self.switchControllersToOperator(type: rechageType)
    }
    @IBAction func selectOperatorIcon(_ sender: UIButton) {
        self.switchControllersToOperator(type: rechageType)
    }
    
    func setUpEvent()  {
        SwiftEventBus.onMainThread(self, name: Constants.Operator_Event) { result in
            let rechargeModel : RechargeModel = result!.object as! RechargeModel
            self.selectedOperator = rechargeModel.operatorName
            let OperatorInnerModel = rechargeModel.operatorInnerModelArrayList
            self.operatorId =  OperatorInnerModel[0].id
            self.selectOperator.text = rechargeModel.operatorName
            print(rechargeModel.operatorName)
        }
        SwiftEventBus.onMainThread(self, name: Constants.selectPlan) { result in
            let planmodel : PlanModel = result!.object as! PlanModel
            self.plan = planmodel.recharge_type!
            self.enteredAmount = planmodel.recharge_amount!
            self.EnterAmount.text = planmodel.recharge_amount!
            self.validity = planmodel.recharge_validity!
            SwiftEventBus.post(Constants.closePlanlisting, sender: nil)
        }
    }
    
    @IBAction func seeplan(_ sender: UIButton) {
        switchControllersToSeePlansDTH(type: type_, circle: selectedCircle, opoperator: selectedOperator)
    }
    
}
