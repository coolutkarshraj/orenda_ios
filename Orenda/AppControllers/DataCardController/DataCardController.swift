//
//  DataCardController.swift
//  Orenda
//
//  Created by Pixel on 04/01/19.
//  Copyright © 2019 Arvind Mehta. All rights reserved.
//

import UIKit
import SwiftEventBus

class DataCardController: BaseViewController,TwicketSegmentedControlDelegate,UITextFieldDelegate {
    @IBOutlet weak var underline: UIView!
    @IBOutlet weak var segmentView: UIView!
    var rechargeType = "1"
    var type_ = "6"
    @IBOutlet weak var segmentControl: UIView!
    
    @IBOutlet weak var enterMobileNumber: UITextField!
    @IBOutlet weak var selectOperator: UILabel!
    @IBOutlet weak var EnterAmount: UITextField!
    @IBAction func seeplanbuttonClick(_ sender: UIButton) {
        if rechargeType == "1"{
            switchControllersToSeePlansData(type: type_, circle: selectedCircle, opoperator: selectedOperator)
        }else if rechargeType == "2"{
            let amount = "10"
            let account = "10"
            let bill_fetch = "1"
            let number = getMyActualPhoneNumber()
            let opIerd = "2"
        }
    }
    @IBOutlet weak var seeplan: UIButton!
    @IBOutlet weak var backmenu: UIImageView!
    
    var selectedOperator = ""
    var selectedCircle = ""
    var plan = ""
    var enteredAmount = ""
    var talkTime = ""
    var validity = ""
    var operatorId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Constants.isViewPlanScreen = "0"
        setupSegment()
        setUpEvent()
        gesterTouch()
        Constants.screenType = "DataCardController"
        enterMobileNumber.delegate = self
    }
    
    
    func setupSegment() {
        let titles = ["Prepaid", "Postpaid"]
        let frame = CGRect(x: 8, y: 8, width: 240, height: 45)
        let segmentedControl = TwicketSegmentedControl(frame:frame)
        segmentedControl.layer.cornerRadius = 7.0
        
        segmentedControl.defaultTextColor = UIColor.white
        segmentedControl.highlightTextColor = UIColor.white
        
        if(Constants.screenType == "PrePaidRechargeScreen"){
            segmentedControl.sliderBackgroundColor = hexStringToUIColor(hex: "ffc101")
            segmentedControl.segmentsBackgroundColor = hexStringToUIColor(hex: "172846")
        }else if(Constants.screenType == "postPaidRechargeScreen"){
            segmentedControl.sliderBackgroundColor = hexStringToUIColor(hex: "172846")
            segmentedControl.segmentsBackgroundColor = hexStringToUIColor(hex: "ffc101")
            
        }else{
            segmentedControl.sliderBackgroundColor = hexStringToUIColor(hex: "ffc101")
            segmentedControl.segmentsBackgroundColor = hexStringToUIColor(hex: "172846")
        }
        segmentedControl.setSegmentItems(titles)
        segmentedControl.delegate = self 
        segmentView.addSubview(segmentedControl)
        
    }
    
    func didSelect(_ segmentIndex: Int) {
        switch segmentIndex {
        case 0:
            rechargeType = "1"
            type_  = "6"
            print("item 1")
        case 1:
            rechargeType = "2"
            type_  = "7"
            print("item 2")
        default:break
            
        }
    }
    
    
    func setUpEvent()  {
        SwiftEventBus.onMainThread(self, name: Constants.Operator_Event) { result in
            let rechargeModel : RechargeModel = result!.object as! RechargeModel
            self.selectedOperator = rechargeModel.operatorName
//            self.operatorId = rechargeModel.operatorId
            let OperatorInnerModel = rechargeModel.operatorInnerModelArrayList
            self.operatorId =  OperatorInnerModel[0].id
            self.selectOperator.text = rechargeModel.operatorName
        }
        SwiftEventBus.onMainThread(self, name: Constants.Circle_Event) { result in
            let rechargeModel : RechargeModel = result!.object as! RechargeModel
            self.selectedCircle = rechargeModel.circleName
            self.selectOperator.text = "\(self.selectedOperator)-\(self.selectedCircle)"
            self.selectOperator.textColor = hexStringToUIColor(hex: "#000000")
            SwiftEventBus.post(Constants.closeCircleListing, sender: nil)
            SwiftEventBus.post(Constants.Close_OperatorListing, sender: nil)
        
            
            
        }
        SwiftEventBus.onMainThread(self, name: Constants.selectPlan) { result in
            let planmodel : PlanModel = result!.object as! PlanModel
            self.plan = planmodel.recharge_type!
            self.enteredAmount = planmodel.recharge_amount!
            self.EnterAmount.text = planmodel.recharge_amount!
            self.validity = planmodel.recharge_validity!
            SwiftEventBus.post(Constants.closePlanlisting, sender: nil)
        }
    }
    
 

    // In a storyboard-based application, you will often want to do a little preparation before navigation
 
 override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    @IBAction func `continue`(_ sender: UIButton) {
        if(Constants.tempAmount == EnterAmount.text!){
            Constants.isViewPlanScreen = Constants.tempIsPlanScreenView
        }
        let ooperator = selectOperator.text
        let ooperatorn = selectOperator.text
        let tfOfSisiter = EnterAmount.text!
        let number  = getMyActualPhoneNumber()
        let opId = operatorId
        let rechtype = type_
        let circle =  "\(ooperatorn!),"
        let selectedplan = plan
        let selectedValidity = validity
        if( ((ooperator?.isEmpty)!) || number.isEmpty || (tfOfSisiter.isEmpty) || rechtype.isEmpty){
            print(".....................emppty"  + ooperator! + "gggg" + number+"hhhhh" + tfOfSisiter, "nnnnn" + rechtype)
        }else{
            if isLogin(){
                navigateScreen()
            }else{
                switchLoginControllersfromRechargeConfirm(tfOfSisiter: tfOfSisiter,number: number,opId: opId,rechtype: rechtype,circle: ooperatorn!,selectedplan: selectedplan,selectedValidity: selectedValidity,rechargeType: rechtype)
            }
            
        }
    }
    @IBAction func selectoperatorFullClick(_ sender: Any) {
        if(rechargeType == "1"){
            self.switchControllersToOperator(type: type_)
        }else{
          self.switchControllersToOperator(type: type_)
        }
        
    }
    @IBAction func selectOperator(_ sender: UIButton) {
        self.switchControllersToOperator(type: rechargeType)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var fullString = textField.text ?? ""
        fullString.append(string)
        print(range.length)
        setMyActualPhoneNumber(phoneNumber: fullString)
        if range.length == 1 {
            enterMobileNumber.text = format(phoneNumber: fullString, shouldRemoveLastDigit: true)
        } else {
            enterMobileNumber.text = format(phoneNumber: fullString)
        }
        if((enterMobileNumber.text?.isPhoneNumber)!){
            mobileNumberLookUp(number: getMyActualPhoneNumber())
        }
        return false
    }
    
    func mobileNumberLookUp(number:String){
        getNumberLookUp(view: self.view, number: number){ responce in
            self.selectedOperator = responce["operator"]!
            self.selectedCircle = responce["circle"]!
            self.selectOperator.text = "\(responce["operator"]!)-\(responce["circle"]!)"
            self.selectOperator.textColor = hexStringToUIColor(hex: "#000000")
            print("\(responce["operator"]!)")
            if(self.rechargeType == "1"){
                self.underline.isHidden = false
                self.seeplan.isHidden = false
            }else if(self.rechargeType == "2"){
                if("\(responce["operator"]!)" == "BSNL"){
                    self.underline.isHidden = false
                    self.seeplan.isHidden = false
                    self.seeplan.setTitle("Fetch Bill", for: .normal)
                }else{
                    self.underline.isHidden = true
                    self.seeplan.isHidden = true
                }
            }
            
            
        }
    }
    
    func navigateScreen(){
        let ooperatorn = selectOperator.text
        
        let tfOfSisiter = EnterAmount.text!
        let number  = getMyActualPhoneNumber()
        let opId = operatorId
        let rechtype = type_
        let circle =  "\(ooperatorn!),"
        let selectedplan = plan
        let selectedValidity = validity
        print(opId)
        if Constants.isViewPlanScreen == "0"{
            print(tfOfSisiter)
            switchControllersToRechargeConfirmWithoutPlan(tfOfSisiter: tfOfSisiter,number: number,opId: opId,rechtype: rechtype,circle: ooperatorn!,selectedplan: selectedplan,selectedValidity: selectedValidity,rechargeType: type_)
            
        }else{
            print("Without Plan")
            print(circle,selectedValidity)
            switchControllersToRechargeConfirm(tfOfSisiter: tfOfSisiter,number: number,opId: opId,rechtype: rechtype,circle: ooperatorn!,selectedplan: selectedplan,selectedValidity: selectedValidity,rechargeType: type_)
            
            
        }
    }
    
    func gesterTouch(){
        let tapdonthaveCode = UITapGestureRecognizer(target: self, action: #selector(PrepaidViewController.tapbackPress))
        backmenu.isUserInteractionEnabled = true
        backmenu.addGestureRecognizer(tapdonthaveCode)
    }
    
    //Action
    @objc func tapbackPress() {
        dismiss(animated: true, completion: nil)
    }
 
}
