//
//  SeeDataCardViewController.swift
//  Orenda
//
//  Created by Pixel on 06/01/19.
//  Copyright © 2019 Arvind Mehta. All rights reserved.
//

import UIKit
import SwiftEventBus

class SeeDataCardViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tableview: UITableView!
    var rechargeType = ""
    var selectedOperator = ""
    var selectedCircle = ""
    var count = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        tableview.delegate = self
        tableview.dataSource = self
        Constants.isViewPlanScreen = "1"
        tableview.register(UINib(nibName: "TPlanViewableViewCell", bundle: nil), forCellReuseIdentifier: "TPlanViewableViewCell")
        
        tableview.estimatedRowHeight = 100
        tableview.rowHeight = UITableView.automaticDimension
        tableview.backgroundColor = .clear

        // Do any additional setup after loading the view.
    }
    var planArray = [PlanModel]()
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return planArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "TPlanViewableViewCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! TPlanViewableViewCell
        cell.selectionStyle = .none
        let planModel = planArray[indexPath.row]
        cell.priceConector.setTitle("Rs. "+planModel.recharge_amount!, for: .normal)
        if((planModel.recharge_validity?.isBlank)!){
            cell.validity.text = "N/A"
        }else{
            cell.validity.text = planModel.recharge_validity
        }
        if((planModel.recharge_talktime == nil)){
            cell.validitydays.text = "N/A"
        }else{
            cell.validitydays.text = planModel.recharge_talktime
        }
        if((planModel.recharge_long_desc?.isBlank)!){
            cell.validitydays.text = ""
        }else{
            cell.DataLimit.text = planModel.recharge_long_desc
        }
        cell.priceConector.tag = indexPath.row
        cell.priceConector.addTarget(self, action: #selector(onItemClicked(sender:)), for: .touchUpInside)
        
        
        return cell
    }
    @objc func onItemClicked(sender:AnyObject) {
        Log(type: "From Select", msg: "Invoked")
        let button = sender as! UIButton
        let row  = button.tag
        SwiftEventBus.post(Constants.selectPlan, sender: planArray[row])
    }
    
    
    func setUp3GPlan(){
        
        getFulltalkTime(view: self.view, count: count, operatorText: selectedOperator, circle: selectedCircle, type: "3g"){ responce in
            
            self.planArray = responce
            self.tableview.reloadData()
        }
    }
    
    func setUp2GPlan(){
        
        getFulltalkTime(view: self.view, count: count, operatorText: selectedOperator, circle: selectedCircle, type: "2g"){ responce in
            
            self.planArray = responce
            self.tableview.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        Log(type: "Appear", msg: self.title!)
        switch self.title! {
        case "2g":
            setUp2GPlan()
        case "3g":
            setUp3GPlan()
        default:break
            
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        SwiftEventBus.post(Constants.selectPlan, sender: planArray[indexPath.row])
    }
    
}
