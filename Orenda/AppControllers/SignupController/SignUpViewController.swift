//
//  SignUpViewController.swift
//  Orenda
//
//  Created by Arvind Mehta on 20/11/18.
//  Copyright © 2018 Arvind Mehta. All rights reserved.
//

import UIKit

class SignUpViewController: BaseViewController,UITextFieldDelegate {

    @IBOutlet weak var loginView: UIView!
    @IBOutlet weak var lb_loginText: UILabel!
    @IBOutlet weak var tf_userPassword: PasswordTextField!
    @IBOutlet weak var tf_phoneNumber: UITextField!
    @IBOutlet weak var tf_userEmail: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        tf_phoneNumber.delegate = self
        
        let tapdonthaveCode = UITapGestureRecognizer(target: self, action: #selector(SignUpViewController.switchLogin))
        loginView.isUserInteractionEnabled = true
        loginView.addGestureRecognizer(tapdonthaveCode)
       
    }
    
    //Action
    @objc func switchLogin() {
        switchControllersToNew(identifier: Constants.LOGIN)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var fullString = textField.text ?? ""
        fullString.append(string)
        setMyActualPhoneNumber(phoneNumber: fullString)
        if range.length == 1 {
            tf_phoneNumber.text = format(phoneNumber: fullString, shouldRemoveLastDigit: true)
        } else {
            tf_phoneNumber.text = format(phoneNumber: fullString)
        }
        return false
    }
    
    func checkVelidation() -> Bool {
        var status = true
        if((tf_userEmail.text?.isBlank)!){
            status = false
            showMessage(titleText: "Error", bodyText: "Please enter email address", msgtype: Constants.ERROR, duration: Constants.DURATION_SHORT)
        }else if(!(tf_userEmail.text?.isValidEmail)!){
            status = false
            showMessage(titleText: "Error", bodyText: "Please enter valid email address", msgtype: Constants.ERROR, duration: Constants.DURATION_SHORT)
        }else if((tf_phoneNumber.text?.isBlank)!){
            status = false
            showMessage(titleText: "Error", bodyText: "Please enter mobile number", msgtype: Constants.ERROR, duration: Constants.DURATION_SHORT)
        }else if(!(tf_phoneNumber.text?.isPhoneNumber)!){
            status = false
            showMessage(titleText: "Error", bodyText: "Mobile number must be of 10 digits", msgtype: Constants.ERROR, duration: Constants.DURATION_SHORT)
        }else if((tf_userPassword.text?.isBlank)!){
            status = false
            showMessage(titleText: "Error", bodyText: "Please enter password", msgtype: Constants.ERROR, duration: Constants.DURATION_SHORT)
        }else if(!(tf_userPassword.text?.isValidateSecialPassword)!){
            status = false
            showMessage(titleText: "Error", bodyText: "Password must of minimum 6 characters and must have atleast 1 capital character ,1 lower character,1 numeric character,1 special character", msgtype: Constants.ERROR, duration: Constants.DURATION_LONG)
        }
        return status
    }
    

    @IBAction func onSubmitClick(_ sender: Any) {
       
      if(checkVelidation()){
        submotData(email: tf_userEmail.text!, number: getMyActualPhoneNumber(), password: tf_userPassword.text!)
        }
    }
    
    func submotData(email:String,number:String,password:String) {
        register(view: self.view, email: email, password: password, mobileNumber: number){ respince in
            //TODO not need to code here all set in Data SVC
            self.switchControllersToNew(identifier: Constants.OTP_CONTROLLER)
        }
    }
    
}
