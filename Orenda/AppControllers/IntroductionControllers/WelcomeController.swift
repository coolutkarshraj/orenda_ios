//
//  StoryboardExampleViewController.swift
//  SwiftyOnboardExample
//
//  Created by Jay on 3/27/17.
//  Copyright © 2017 Juan Pablo Fernandez. All rights reserved.
//

import UIKit


class WelcomeController: BaseViewController {

    @IBOutlet weak var swiftyOnboard: SwiftyOnboard!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        swiftyOnboard.style = .light
        swiftyOnboard.delegate = self
        swiftyOnboard.dataSource = self
        swiftyOnboard.backgroundColor = UIColor(red: 46/256, green: 46/256, blue: 76/256, alpha: 1)
    }
    
    @objc func handleSkip() {
        swiftyOnboard?.goToPage(index: 2, animated: true)
    }
    
    @objc func handleContinue(sender: UIButton) {
        let index = sender.tag
        if(index == 2){
            if(isLogin()){
                switchControllersToNew(identifier: Constants.SWRevealViewController)
            }else{
                   switchControllersToNew(identifier: Constants.SWRevealViewController)
            }
         
        }else{
            swiftyOnboard?.goToPage(index: index + 1, animated: true)
        }
        
    }
    
    
}
extension WelcomeController: SwiftyOnboardDelegate, SwiftyOnboardDataSource {
    
    func swiftyOnboardNumberOfPages(_ swiftyOnboard: SwiftyOnboard) -> Int {
        return 3
    }
    
    func swiftyOnboardPageForIndex(_ swiftyOnboard: SwiftyOnboard, index: Int) -> SwiftyOnboardPage? {
        let view = CustomPage.instanceFromNib() as? CustomPage
        switch index {
        case 0:
            view?.image.image = UIImage(named: "introOne")
            view?.hoverImage.image = UIImage(named: "introHoverOne")
            view?.image.contentMode = .scaleToFill
            view?.hoverImage.contentMode = .scaleAspectFit
        case 1:
            view?.image.image = UIImage(named: "introTwo")
            view?.hoverImage.image = UIImage(named: "introHoverTwo")
            view?.image.contentMode = .scaleToFill
            view?.hoverImage.contentMode = .scaleAspectFit
        case 2:
            view?.image.image = UIImage(named: "introThree")
            view?.hoverImage.image = UIImage(named: "introHoverThree")
            view?.image.contentMode = .scaleToFill
            view?.hoverImage.contentMode = .scaleAspectFit
        default:break
            
        }
        return view
    }
    
    func swiftyOnboardViewForOverlay(_ swiftyOnboard: SwiftyOnboard) -> SwiftyOnboardOverlay? {
        let overlay = CustomOverlay.instanceFromNib() as? CustomOverlay
        overlay?.skip.addTarget(self, action: #selector(handleSkip), for: .touchUpInside)
        overlay?.buttonContinue.addTarget(self, action: #selector(handleContinue), for: .touchUpInside)
        return overlay
    }
    
    func swiftyOnboardOverlayForPosition(_ swiftyOnboard: SwiftyOnboard, overlay: SwiftyOnboardOverlay, for position: Double) {
        let overlay = overlay as! CustomOverlay
        let currentPage = round(position)
        print(Int(currentPage))
        overlay.pageController.currentPage = Int(currentPage)
        overlay.buttonContinue.tag = Int(position)
        if currentPage == 0.0 || currentPage == 1.0 {
            overlay.buttonContinue.setTitle("Continue", for: .normal)
            overlay.skip.setTitle("Skip", for: .normal)
            overlay.skip.isHidden = false
        } else {
            overlay.buttonContinue.setTitle("Get Started!", for: .normal)
            overlay.skip.isHidden = true
        }
    }
}


