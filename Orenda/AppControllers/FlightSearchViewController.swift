//
//  FlightSearchViewController.swift
//  ConnectAdlinks
//
//  Created by Arvind Mehta on 26/11/18.
//  Copyright © 2018 Pixel. All rights reserved.
//

import UIKit

class FlightSearchViewController: UIViewController,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searcArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           let cellIdentifier = "SearchTableViewCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! SearchTableViewCell
        cell.lbSearch.text = "\(String(describing: searcArray[indexPath.row].city!)) (\(String(describing: searcArray[indexPath.row].airport_code!)), \(String(describing: searcArray[indexPath.row].country!)))"

        
        return cell
    
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        if(titleShow == "Source"){
           flightController?.setSourceFields(source: searcArray[indexPath.row])
        }else{
             flightController?.setDestinationFields(source: searcArray[indexPath.row])
        }
        dismiss(animated: true, completion: nil)
      
    }
    
    @IBOutlet weak var lb_title: UILabel!
    
    @IBOutlet weak var searchTableView: UITableView!
    @IBOutlet weak var tf_cityName: UITextField!
   // var progress:MBProgressHUD? = nil
    var titleShow:String? = ""
    var flightController:FlightViewController? = nil
    var searcArray = [SearchReselt]()
    override func viewDidLoad() {
        super.viewDidLoad()
//        progress = MBProgressHUD.init()
//        self.view.addSubview(progress!)
//        progress?.delegate = self
         tf_cityName.delegate = self
        searchTableView.delegate = self
        searchTableView.dataSource = self
    
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let fullString = textField.text ?? ""
        getCity(city: fullString)
       
        return true
    }
    

    @IBAction func onBackPressClick(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    func getCity(city:String)  {
      //  self.progress?.show(animated: true)
        let url = "https://www.mukh.pixelsoftwares.com/flyeasytrip/inception/getairport"
        let encodedUrl = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlFragmentAllowed)
    
        let param = ["user_id":"54600",
                     "airportname":city,
                     "flighttype":"2"]
       
        
        executePOSTConnectAdd(view: self.view, path: encodedUrl!, parameter: param){ responce in
            if(responce["status"].intValue == 200){
                                self.searcArray.removeAll()
                
                                for data in responce["data"].arrayValue{
                                     self.searcArray.append(SearchReselt(json: data))
                                }
                
                            }
                            self.searchTableView.reloadData()
            
        }
      
//        APIManager.init().postRequest(withUrlHeader: encodedUrl, withParameter: parameter, complitation: { (json) -> Void in
//
//            let swifyJson = JSON(json!)
//            self.progress?.hide(animated: true)
//            if(swifyJson["status"].intValue == 200){
//                self.searcArray.removeAll()
//
//                for data in swifyJson["data"].arrayValue{
//                     self.searcArray.append(SearchReselt(json: data))
//                }
//
//            }
//            self.searchTableView.reloadData()
//
//            print(swifyJson)
//
//        }, failure: {(error: Error!) in
//
//        })
        }
}
