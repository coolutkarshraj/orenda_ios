//
//  ForgotViewController.swift
//  Orenda
//
//  Created by Arvind Mehta on 30/11/18.
//  Copyright © 2018 Arvind Mehta. All rights reserved.
//

import UIKit

class ForgotViewController: UIViewController {

    @IBOutlet weak var tf_confirmPassword: UITextField!
    @IBOutlet weak var tf_newPassword: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func verifyFieds() -> String  {
        var message = ""
        if((tf_confirmPassword.text?.isBlank)!){
            message = "Please enter your confirm password"
        }else if((tf_newPassword.text?.isBlank)!){
            message = "Please enter your confirm password"
        }
        return message
    }
    

    @IBAction func onresetclick(_ sender: Any) {
        if(verifyFieds() != ""){
              showMessage(titleText: "Error", bodyText: verifyFieds(), msgtype: Constants.ERROR, duration: Constants.DURATION_MIDIUM)
        }else{
            
        }
    }
    
    func rest(){
        
    }
    

}
