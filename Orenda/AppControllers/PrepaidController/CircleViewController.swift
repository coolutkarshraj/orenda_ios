//
//  CircleViewController.swift
//  Orenda
//
//  Created by Arvind Mehta on 02/12/18.
//  Copyright © 2018 Arvind Mehta. All rights reserved.
//

import UIKit
import SwiftEventBus

class CircleViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return circleArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "CircleTableViewCell"
          let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! CircleTableViewCell
        cell.lbName.text = circleArray[indexPath.row].circleName
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         SwiftEventBus.post(Constants.Circle_Event, sender: circleArray[indexPath.row])
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBOutlet weak var backMenu: UIImageView!
    
    @IBOutlet weak var controllerTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var rechargeType = ""
    var circleArray = [RechargeModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        getOperatorcircle()
        gesterTouch()
        // Do any additional setup after loading the view.
    }
    
    func setUpEvent()  {
        SwiftEventBus.onMainThread(self, name: Constants.closeCircleListing) { result in
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    func gesterTouch(){
        let tapdonthaveCode = UITapGestureRecognizer(target: self, action: #selector(OperatorViewController.tapbackPress))
        backMenu.isUserInteractionEnabled = true
        backMenu.addGestureRecognizer(tapdonthaveCode)
    }
    
    //Action
    @objc func tapbackPress() {
        dismiss(animated: true, completion: nil)
    }
    
    func getOperatorcircle()  {
        getcircleList(view: self.view, type:rechargeType){ responce in
            if(!responce.isEmpty){
                self.circleArray = responce
            }else{
                 showMessage(titleText: "Failed", bodyText: "Unable to fetch data", msgtype: Constants.ERROR, duration: Constants.DURATION_SHORT)
            }
            self.tableView.reloadData()
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        SwiftEventBus.unregister(self)
    }


}
