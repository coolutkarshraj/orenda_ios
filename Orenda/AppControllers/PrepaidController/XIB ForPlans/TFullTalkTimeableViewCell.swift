//
//  TFullTalkTimeableViewCell.swift
//  Orenda
//
//  Created by Arvind Mehta on 02/12/18.
//  Copyright © 2018 Arvind Mehta. All rights reserved.
//

import UIKit

class TFullTalkTimeableViewCell: UITableViewCell {

    @IBOutlet weak var btconnector: UIButton!
    @IBOutlet weak var lbDesc: UILabel!
    @IBOutlet weak var btPrice: UIButton!
    @IBOutlet weak var lbValidity: UILabel!
    @IBOutlet weak var lbTalktime: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
