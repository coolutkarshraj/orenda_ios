//
//  SideMenuViewController.swift
//  Orenda
//
//  Created by Arvind Mehta on 23/11/18.
//  Copyright © 2018 Arvind Mehta. All rights reserved.
//

import UIKit

class SideMenuViewController: BaseViewController {
   
   
    @IBOutlet weak var labelWallet: UILabel!
    @IBOutlet weak var imageviewWallet: UIImageView!
    
    @IBOutlet weak var labelAddMoney: UILabel!
    
    @IBOutlet weak var IMAGEVIEWADDMONEY: UIImageView!
   
    @IBOutlet weak var imageviewRedeemvoucher: UIImageView!
    
    @IBAction func doSignup(_ sender: UIButton) {
        print("SignUp................")
         switchControllersToNew(identifier: Constants.REGISTER)
      

    }
    @IBAction func doLogin(_ sender: UIButton) {
        print("Login................")
         switchControllersToNew(identifier: Constants.LOGIN)
        

    }
    @IBOutlet weak var labelredeemMeVoucher: UILabel!
    
    @IBOutlet weak var LabelNotification: UILabel!
    @IBOutlet weak var notificationImageView: UIImageView!
    @IBOutlet weak var imageViewhelpSupport: UIImageView!
   
    
    @IBOutlet weak var labelhelpSupport: UILabel!
    @IBOutlet weak var logoutSingleLine: UIView!
    @IBOutlet weak var logoutUiView: UIView!
    @IBOutlet weak var CashBackPolicy: UIView!
    @IBOutlet weak var cashbackUiPolicy: UIView!
    @IBOutlet weak var aboutUs: UIView!
    @IBOutlet weak var RefundPolicy: UIView!
    @IBOutlet weak var tnc: UIView!
    @IBOutlet weak var LoginSignupView: UIView!
    @IBOutlet weak var LoginSignupSingleLine: UIView!
    @IBOutlet weak var NotificationNewLine: UIView!
    @IBOutlet weak var beforeLogoutLine: UIView!
    @IBOutlet weak var logutView: UIView!
    @IBOutlet weak var MyAccountView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        if isLogin(){
            LoginSignupView.removeFromSuperview()
            LoginSignupSingleLine.removeFromSuperview()
            
            
        }else{
             MyAccountView.removeFromSuperview()
             aboutUs.removeFromSuperview()
             RefundPolicy.removeFromSuperview()
             tnc.removeFromSuperview()
             NotificationNewLine.removeFromSuperview()
            CashBackPolicy.removeFromSuperview()
            cashbackUiPolicy.removeFromSuperview()
            logoutUiView.removeFromSuperview()
            logoutSingleLine.removeFromSuperview()
            self.labelWallet.text = "Help & Support"
            self.labelAddMoney.text = "About Us"
            self.labelredeemMeVoucher.text = "Refund Policy"
            self.LabelNotification.text = "Cashback Policy"
            self.labelhelpSupport.text = "Terms & Conditions"
            let yourImage: UIImage = UIImage(named: "help_side_menu")!
            imageviewWallet.image = yourImage
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
