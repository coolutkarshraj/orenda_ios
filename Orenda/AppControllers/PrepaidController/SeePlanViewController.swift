//
//  SeePlanViewController.swift
//  Orenda
//
//  Created by Utkarsh Raj on 01/01/19.
//  Copyright © 2018 Utkarsh Raj. All rights reserved.
//

import UIKit
import Parchment
import SwiftEventBus

class SeePlanViewController: BaseViewController {

    @IBOutlet weak var backMenu: UIImageView!
    @IBOutlet weak var controllerTitle: UILabel!
    @IBOutlet weak var container: UIView!
    var rechargeType = ""
    var selectedOperator = ""
    var selectedCircle = ""
    override func viewDidLoad() {
        super.viewDidLoad()
            gesterTouch()

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let firstViewController = storyboard.instantiateViewController(withIdentifier: "PlanViewController") as! PlanViewController
        let secondViewController = storyboard.instantiateViewController(withIdentifier: "PlanViewController") as! PlanViewController
        let thirdViewController = storyboard.instantiateViewController(withIdentifier: "PlanViewController") as! PlanViewController
        let fourthViewController = storyboard.instantiateViewController(withIdentifier: "PlanViewController") as! PlanViewController
        let fiveViewController = storyboard.instantiateViewController(withIdentifier: "PlanViewController") as! PlanViewController
        let sixViewController = storyboard.instantiateViewController(withIdentifier: "PlanViewController") as! PlanViewController
        let sevenViewController = storyboard.instantiateViewController(withIdentifier: "PlanViewController") as! PlanViewController
        
        firstViewController.title = "Full Talk Time"
        secondViewController.title = "4g"
        thirdViewController.title = "3g"
        fourthViewController.title = "2g"
        fiveViewController.title = "Top up"
        sixViewController.title = "Special Recharge"
        sevenViewController.title = "Roaming"
        
        firstViewController.rechargeType = rechargeType
        firstViewController.selectedCircle = selectedCircle
        firstViewController.selectedOperator = selectedOperator
        
        secondViewController.rechargeType = rechargeType
        secondViewController.selectedCircle = selectedCircle
        secondViewController.selectedOperator = selectedOperator
        
        thirdViewController.rechargeType = rechargeType
        thirdViewController.selectedCircle = selectedCircle
        thirdViewController.selectedOperator = selectedOperator
        
        fourthViewController.rechargeType = rechargeType
        fourthViewController.selectedCircle = selectedCircle
        fourthViewController.selectedOperator = selectedOperator
        
        fiveViewController.rechargeType = rechargeType
        fiveViewController.selectedCircle = selectedCircle
        fiveViewController.selectedOperator = selectedOperator
        
        sixViewController.rechargeType = rechargeType
        sixViewController.selectedCircle = selectedCircle
        sixViewController.selectedOperator = selectedOperator
        
        sevenViewController.rechargeType = rechargeType
        sevenViewController.selectedCircle = selectedCircle
        sevenViewController.selectedOperator = selectedOperator
        
    
        
        
        
        // Initialize a FixedPagingViewController and pass
        // in the view controllers.
        let pagingViewController = FixedPagingViewController(viewControllers: [
            firstViewController,
            secondViewController,
            thirdViewController,
            fourthViewController,
            fiveViewController,
            sixViewController,
            sevenViewController
            ])
        
        // Make sure you add the PagingViewController as a child view
        // controller and contrain it to the edges of the view.
        addChild(pagingViewController)
        container.addSubview(pagingViewController.view)
        container.constrainToEdges(pagingViewController.view)
        pagingViewController.didMove(toParent: self)
        
        SwiftEventBus.onMainThread(self, name: Constants.closePlanlisting) { result in
           self.dismiss(animated: true, completion: nil)
        }
        
        
        
    }

    func gesterTouch(){
        let tapdonthaveCode = UITapGestureRecognizer(target: self, action: #selector(SeePlanViewController.tapbackPress))
        backMenu.isUserInteractionEnabled = true
        backMenu.addGestureRecognizer(tapdonthaveCode)
    }
    
    //Action
    @objc func tapbackPress() {
        Constants.isViewPlanScreen = "0"
        dismiss(animated: true, completion: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        SwiftEventBus.unregister(self)
    }
    
    
    

}
