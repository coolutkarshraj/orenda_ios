//
//  ConfirmRechargeWithoutPlanViewController.swift
//  Orenda
//
//  Created by Pixel on 01/01/19.
//  Copyright © 2019 Arvind Mehta. All rights reserved.
//

import UIKit

class ConfirmRechargeWithoutPlanViewController: BaseViewController {
    @IBOutlet weak var mobilenumberlabel: UILabel!
    @IBOutlet weak var lboperatorCircle: UILabel!
    @IBOutlet weak var lbAmount: UILabel!
    @IBOutlet weak var TotalAmount: UILabel!
    @IBOutlet weak var lbpromocode: UILabel!
    var plan = ""
    var talkTime = ""
    var validity = ""
    var opoperator = ""
    var mobile = ""
    var amount = ""
    var type = ""
    var operator_id = ""
    var rupeesSign = "INR"
    
    @IBOutlet weak var imgBackMenu: UIImageView!
    @IBAction func continuetoPayment(_ sender: UIButton) {
    
                 self.switchpaymentConfirmation(tfOfSisiter: self.amount,number: self.mobile,opId: self.operator_id,rechtype: self.type,circle: self.opoperator,selectedplan: self.plan,selectedValidity: self.talkTime,rechargeType: self.type)
    }
    @IBOutlet weak var proceedToPaymentController: UIButton!
    
    @IBOutlet weak var btnContinue: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(mobile)
        gesterTouch()
        TotalAmount.text = "\(rupeesSign) \(amount)"
        lbAmount.text = "\(rupeesSign) \(amount)"
        lboperatorCircle.text = opoperator
        lbpromocode.text = rupeesSign
        mobilenumberlabel.text = mobile

        // Do any additional setup after loading the view.
    }
    
    func gesterTouch(){
        let tapdonthaveCode = UITapGestureRecognizer(target: self, action: #selector(PrepaidViewController.tapbackPress))
        imgBackMenu.isUserInteractionEnabled = true
        imgBackMenu.addGestureRecognizer(tapdonthaveCode)
    }
    
    //Action
    @objc func tapbackPress() {
        Constants.tempIsPlanScreenView = Constants.isViewPlanScreen
        Constants.isViewPlanScreen = "0"
        dismiss(animated: true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

 
}
