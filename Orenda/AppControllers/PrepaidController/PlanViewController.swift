//
//  PlanViewController.swift
//  Orenda
//
//  Created by Arvind Mehta on 02/12/18.
//  Copyright © 2018 Arvind Mehta. All rights reserved.
//

import UIKit
import SwiftEventBus

class PlanViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource{
    var rechargeType = ""
    var selectedOperator = ""
    var selectedCircle = ""
    var count = 0
    
    
    var planArray = [PlanModel]()
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return planArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "TFullTalkTimeableViewCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! TFullTalkTimeableViewCell
        cell.selectionStyle = .none
        let planModel = planArray[indexPath.row]
        cell.btPrice.setTitle("Rs. "+planModel.recharge_amount!, for: .normal)
        if((planModel.recharge_validity?.isBlank)!){
            cell.lbValidity.text = "N/A"
        }else{
            cell.lbValidity.text = planModel.recharge_validity
        }
        if((planModel.recharge_talktime == nil)){
            cell.lbTalktime.text = "N/A"
        }else{
             cell.lbTalktime.text = planModel.recharge_talktime
        }
        if((planModel.recharge_long_desc?.isBlank)!){
            cell.lbDesc.text = ""
        }else{
             cell.lbDesc.text = planModel.recharge_long_desc
        }
        cell.btconnector.tag = indexPath.row
        cell.btconnector.addTarget(self, action: #selector(onItemClicked(sender:)), for: .touchUpInside)
       
        
        return cell
    }
    
    
    @objc func onItemClicked(sender:AnyObject) {
        Log(type: "From Select", msg: "Invoked")
        let button = sender as! UIButton
        let row  = button.tag
         SwiftEventBus.post(Constants.selectPlan, sender: planArray[row])
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         SwiftEventBus.post(Constants.selectPlan, sender: planArray[indexPath.row])
    }
    

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Constants.isViewPlanScreen = "1"
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "TFullTalkTimeableViewCell", bundle: nil), forCellReuseIdentifier: "TFullTalkTimeableViewCell")
        
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableView.automaticDimension
        tableView.backgroundColor = .clear
    

      
    }
    
    override func viewWillAppear(_ animated: Bool) {
        Log(type: "Appear", msg: self.title!)
        switch self.title! {
        case "Full Talk Time":
            setUpFulltalkTime()
        case "4g":
            setUp4GPlan()
        case "3g":
            setUp3GPlan()
        case "2g":
            setUp2GPlan()
        case "Top up":
            setUpTopPlan()
        case "Special Recharge":
            setUpSpecialPlan()
        case "Roaming":
            setUpRoamingPlan()
        default:break
            
        }
    }
    
    func setUpFulltalkTime(){
        
        getFulltalkTime(view: self.view, count: count, operatorText: selectedOperator, circle: selectedCircle, type: "full"){ responce in
            
            self.planArray = responce
            self.tableView.reloadData()
        }
    }
    
    func setUp4GPlan(){
        
        getFulltalkTime(view: self.view, count: count, operatorText: selectedOperator, circle: selectedCircle, type: "4g"){ responce in
            
            self.planArray = responce
            self.tableView.reloadData()
        }
    }
    
    func setUp3GPlan(){
        
        getFulltalkTime(view: self.view, count: count, operatorText: selectedOperator, circle: selectedCircle, type: "3g"){ responce in
            
            self.planArray = responce
            self.tableView.reloadData()
        }
    }
    
    func setUp2GPlan(){
        
        getFulltalkTime(view: self.view, count: count, operatorText: selectedOperator, circle: selectedCircle, type: "2g"){ responce in
            
            self.planArray = responce
            self.tableView.reloadData()
        }
    }
    func setUpTopPlan(){
        
        getFulltalkTime(view: self.view, count: count, operatorText: selectedOperator, circle: selectedCircle, type: "top"){ responce in
            
            self.planArray = responce
            self.tableView.reloadData()
        }
    }
    func setUpSpecialPlan(){
        
        getFulltalkTime(view: self.view, count: count, operatorText: selectedOperator, circle: selectedCircle, type: "Other"){ responce in
            
            self.planArray = responce
            self.tableView.reloadData()
        }
    }
    func setUpRoamingPlan(){
        
        getFulltalkTime(view: self.view, count: count, operatorText: selectedOperator, circle: selectedCircle, type: "Roaming"){ responce in
            
            self.planArray = responce
            self.tableView.reloadData()
        }
    }
    
}
