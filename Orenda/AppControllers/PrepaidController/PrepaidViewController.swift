//
//  PrepaidViewController.swift
//  Orenda
//
//  Created by Utkarsh Raj on 29/12/18.
//  Copyright © 2018 Utkarsh Raj. All rights reserved.
//

import UIKit
import EPContactsPicker
import SwiftEventBus

class PrepaidViewController: BaseViewController ,TwicketSegmentedControlDelegate,EPPickerDelegate,UITextFieldDelegate{

    @IBOutlet weak var imagebackController: UIImageView!
    @IBAction func rechargeConfirmation(_ sender: UIButton) {
        print("Proceed to recharge")
    }
    @IBOutlet weak var controllerTitle: UILabel!
    @IBOutlet weak var underLine: UIView!
    @IBOutlet weak var imgBackMenu: UIImageView!

   
    @IBOutlet weak var btSeePlan: UIButton!
    @IBOutlet weak var tfAmount: UITextField!
    @IBOutlet weak var lbselectOperator: UILabel!
    
   
    @IBOutlet weak var tfNumber: UITextField!
    @IBOutlet weak var segmentView: UIView!
     var operatorList = [RechargeModel]()
    var selectedOperator = ""
    var selectedCircle = ""
    var rechargeType = "1"
    var plan = ""
    var enteredAmount = ""
    var talkTime = ""
    var validity = ""
    var operatorId = ""
    var position = 0

    
    override func viewDidLoad() {
        super.viewDidLoad()
        tfNumber.delegate = self
        btSeePlan.isHidden = true
        underLine.isHidden = true
        Constants.isViewPlanScreen = "0"
        if(Constants.screenType == "PrePaidRechargeScreen"){
             controllerTitle.text = "Mobile Prepaid"
             rechargeType = "1"
            getOperators()
        }else if(Constants.screenType == "postPaidRechargeScreen"){
             controllerTitle.text = "Mobile Postpaid"
            rechargeType = "2"
            getOperators()
        }else{
            controllerTitle.text = "Mobile Prepaid"
            rechargeType = "1"
            getOperators()
        }
        gesterTouch()
        setupSegment()
        setUpEvent()
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var fullString = textField.text ?? ""
        fullString.append(string)
        print(range.length)
         setMyActualPhoneNumber(phoneNumber: fullString)
        if range.length == 1 {
            tfNumber.text = format(phoneNumber: fullString, shouldRemoveLastDigit: true)
        } else {
            tfNumber.text = format(phoneNumber: fullString)
        }
        if((tfNumber.text?.isPhoneNumber)!){
           mobileNumberLookUp(number: getMyActualPhoneNumber())
        }
        return false
    }
    
    func setUpEvent()  {
        SwiftEventBus.onMainThread(self, name: Constants.Operator_Event) { result in
            let rechargeModel : RechargeModel = result!.object as! RechargeModel
            self.selectedOperator = rechargeModel.operatorName
            self.lbselectOperator.text = rechargeModel.operatorName
            let OperatorInnerModel = rechargeModel.operatorInnerModelArrayList
            self.operatorId =  OperatorInnerModel[0].id
            print(self.operatorId)
            self.lbselectOperator.textColor = hexStringToUIColor(hex: "#000000")
        }
        SwiftEventBus.onMainThread(self, name: Constants.Circle_Event) { result in
            let rechargeModel : RechargeModel = result!.object as! RechargeModel
            self.selectedCircle = rechargeModel.circleName
            self.lbselectOperator.text = "\(self.selectedOperator)-\(self.selectedCircle)"
            self.lbselectOperator.textColor = hexStringToUIColor(hex: "#000000")
             SwiftEventBus.post(Constants.closeCircleListing, sender: nil)
             SwiftEventBus.post(Constants.Close_OperatorListing, sender: nil)
             self.underLine.isHidden = false
        
            self.btSeePlan.isHidden = false
       
            
           
        }
        SwiftEventBus.onMainThread(self, name: Constants.selectPlan) { result in
            let planmodel : PlanModel = result!.object as! PlanModel
            self.tfAmount.text = planmodel.recharge_amount
            self.plan = planmodel.recharge_type!
            self.enteredAmount = planmodel.recharge_amount!
            self.talkTime = planmodel.recharge_talktime!
            self.validity = planmodel.recharge_validity!
             SwiftEventBus.post(Constants.closePlanlisting, sender: nil)
        }
    }
    

    func setupSegment() {
        let titles = ["Prepaid", "Postpaid"]
        let frame = CGRect(x: 8, y: 8, width: 240, height: 45)
        let segmentedControl = TwicketSegmentedControl(frame:frame)
        segmentedControl.layer.cornerRadius = 7.0
       
        segmentedControl.defaultTextColor = UIColor.white
        segmentedControl.highlightTextColor = UIColor.white
       
        if(Constants.screenType == "PrePaidRechargeScreen"){
             segmentedControl.sliderBackgroundColor = hexStringToUIColor(hex: "ffc101")
             segmentedControl.segmentsBackgroundColor = hexStringToUIColor(hex: "172846")
        }else if(Constants.screenType == "postPaidRechargeScreen"){
             segmentedControl.sliderBackgroundColor = hexStringToUIColor(hex: "172846")
             segmentedControl.segmentsBackgroundColor = hexStringToUIColor(hex: "ffc101")
           
        }
        
        segmentedControl.setSegmentItems(titles)
        segmentedControl.delegate = self
        segmentView.addSubview(segmentedControl)
        
    }
    
    
    
    func didSelect(_ segmentIndex: Int) {
        //Segment Select
        switch segmentIndex {
        case 0:
            Constants.screenType = "PrePaidRechargeScreen"
            switchControllersToNew(identifier: Constants.PREPAID_CONTROLLER)
            self.controllerTitle.text = "Mobile Prepaid"
            self.rechargeType = "1"
        case 1:
            Constants.screenType = "postPaidRechargeScreen"
            switchControllersToNew(identifier: Constants.PREPAID_CONTROLLER)
            self.controllerTitle.text = "Mobile Postpaid"
            self.rechargeType = "2"
        default:break
            
        }
    }
    
       //MARK: EPContactsPicker delegates
    @IBAction func onShowContactClick(_ sender: Any) {
        let contactPickerScene = EPContactsPicker(delegate: self, multiSelection:false, subtitleCellType: SubtitleCellValue.email)
        let navigationController = UINavigationController(rootViewController: contactPickerScene)
        self.present(navigationController, animated: true, completion: nil)
    }
 
    func epContactPicker(_: EPContactsPicker, didContactFetchFailed error : NSError)
    {
        print("Failed with error \(error.description)")
    }
    
    func epContactPicker(_: EPContactsPicker, didSelectContact contact : EPContact)
    {
        var phone = contact.phoneNumbers[0].phoneNumber.replacingOccurrences(of: "-", with: " ")
        phone = phone.replacingOccurrences(of: "-", with: "")
        print("Contact \(phone) has been selected")
        setMyActualPhoneNumber(phoneNumber: phone)
        tfNumber.text = format(phoneNumber: phone)
        
    }
    
    func epContactPicker(_: EPContactsPicker, didCancel error : NSError)
    {
        print("User canceled the selection");
    }
    
    func epContactPicker(_: EPContactsPicker, didSelectMultipleContacts contacts: [EPContact]) {
        print("The following contacts are selected")
        for contact in contacts {
            print("\(contact.displayName())")
        }
    }

    
    @IBAction func onDropDownClick(_ sender: Any) {
       
       
    }
   
  
    @IBAction func onSeePlanClick(_ sender: Any) {
        if rechargeType == "1"{
             switchControllersToSeePlans(type: "1", circle: selectedCircle, opoperator: selectedOperator)
        }else if rechargeType == "2"{
            let amount = "10"
            let account = "10"
            let bill_fetch = "1"
            let number = getMyActualPhoneNumber()
            let opIerd = "2"
      getBillData(mobile:number,amount:amount,operatorId:opIerd,rechargeType:self.rechargeType,account:account,bill_fetch:bill_fetch)
        }
        
    }
    
    func getBillData(mobile:String,amount:String,operatorId:String,rechargeType:String,account:String,bill_fetch:String)  {
        callGetBillData(view: self.view, mobile: mobile, amount: amount,optId:operatorId,rectype:rechargeType,account:account,bill_fetch:bill_fetch){response in
            Log(type: "PostpaidFetchBill", msg: response)
            if(response["error_code"].intValue == 400){
                showMessage(titleText: "Error", bodyText: response["message"].stringValue, msgtype: Constants.WARNING, duration: Constants.DURATION_MIDIUM)
            }else {
                print("response......................")
                var bill = response["data"]["bill"]["amount"].stringValue
                print(bill)
                self.tfAmount.text = bill
                showMessage(titleText: "Success", bodyText: response["message"].stringValue, msgtype: Constants.SUCCESS, duration: Constants.DURATION_MIDIUM)

                
                
            }
        }
    }
    func getOperators()
    {
        getOperatorList(view: self.view, type: rechargeType){responce in
            self.operatorList = responce
              print(self.operatorList)
        
            
        }
    }
    
    
    @IBAction func onoperatorselect(_ sender: Any) {
        if((tfNumber.text?.isBlank)!){
            showMessage(titleText: "Error", bodyText: "Please enter mobile number", msgtype: Constants.ERROR, duration: Constants.DURATION_SHORT)
        }else if(!(tfNumber.text?.isPhoneNumber)!){
            showMessage(titleText: "Error", bodyText: "Mobile number must be of 10 digits", msgtype: Constants.ERROR, duration: Constants.DURATION_SHORT)
        }else{
            switchControllersToOperator(type: rechargeType)
        }
    }
    @IBAction func onContinueClick(_ sender: Any) {
        if(Constants.tempAmount == tfAmount.text!){
            Constants.isViewPlanScreen = Constants.tempIsPlanScreenView
        }
        let ooperator = lbselectOperator.text
        let ooperatorn = lbselectOperator.text
        let tfOfSisiter = tfAmount.text!
        let number  = getMyActualPhoneNumber()
        let opId = operatorId
        let rechtype = rechargeType
        let circle =  "\(ooperatorn!),"
        let selectedplan = plan
        let selectedValidity = validity
        if( ((ooperator?.isEmpty)!) || number.isEmpty || (tfOfSisiter.isEmpty) || rechtype.isEmpty){
            print(".....................emppty"  + ooperator! + "gggg" + number+"hhhhh" + tfOfSisiter, "nnnnn" + rechtype)
        }else{
            
            if isLogin(){
            
               navigateScreen()
            }else{
                
               
                  switchLoginControllersfromRechargeConfirm(tfOfSisiter: tfOfSisiter,number: number,opId: opId,rechtype: rechtype,circle: ooperatorn!,selectedplan: selectedplan,selectedValidity: selectedValidity,rechargeType: rechargeType)
            }
            
        }
 }
    func mobileNumberLookUp(number:String){
        getNumberLookUp(view: self.view, number: number){ responce in
            self.selectedOperator = responce["operator"]!
            self.selectedCircle = responce["circle"]!
            self.lbselectOperator.text = "\(responce["operator"]!)-\(responce["circle"]!)"
            print(self.operatorList)
            self.lbselectOperator.textColor = hexStringToUIColor(hex: "#000000")
            print("\(responce["operator"]!)")
            print(self.operatorList)
            
            for operatorList in self.operatorList{
                let type = self.operatorList[self.position].operatorName
        
                 print(type)
                if(type == responce["operator"]! ){
                    let OperatorInnerModel = self.operatorList[self.position].operatorInnerModelArrayList
                    self.operatorId =  OperatorInnerModel[0].id
                    print(self.operatorId)
                    print("helloiii")
                }
                self.position += 1
            }

           
            if(self.rechargeType == "1"){
                self.underLine.isHidden = false
                self.btSeePlan.isHidden = false
            }else if(self.rechargeType == "2"){
                if("\(responce["operator"]!)" == "BSNL"){
                    self.underLine.isHidden = false
                    self.btSeePlan.isHidden = false
                 self.btSeePlan.setTitle("Fetch Bill", for: .normal)
                }else{
                    self.underLine.isHidden = true
                    self.btSeePlan.isHidden = true
                }
            }
        
           
        }
    }
    
    
    func navigateScreen(){
        let ooperatorn = lbselectOperator.text
        let tfOfSisiter = tfAmount.text!
        let number  = getMyActualPhoneNumber()
        let opId = operatorId
        let rechtype = rechargeType
        let circle =  "\(ooperatorn!),"
        let selectedplan = plan
        let selectedValidity = validity
        print(opId)
        print(selectedValidity)
        if Constants.isViewPlanScreen == "0"{
            print(tfOfSisiter)
            switchControllersToRechargeConfirmWithoutPlan(tfOfSisiter: tfOfSisiter,number: number,opId: opId,rechtype: rechtype,circle: ooperatorn!,selectedplan: selectedplan,selectedValidity: selectedValidity,rechargeType: rechargeType)
            
        }else{
            print("Without Plan")
              print(circle,selectedValidity,opId)
            switchControllersToRechargeConfirm(tfOfSisiter: tfOfSisiter,number: number,opId: opId,rechtype: rechtype,circle: ooperatorn!,selectedplan: selectedplan,selectedValidity: selectedValidity,rechargeType: rechargeType)
            
           
        }
    }
    func gesterTouch(){
        let tapdonthaveCode = UITapGestureRecognizer(target: self, action: #selector(PrepaidViewController.tapbackPress))
        imgBackMenu.isUserInteractionEnabled = true
        imgBackMenu.addGestureRecognizer(tapdonthaveCode)
    }
    
    //Action
    @objc func tapbackPress() {
        dismiss(animated: true, completion: nil)
    }
    
   
    
    
}
