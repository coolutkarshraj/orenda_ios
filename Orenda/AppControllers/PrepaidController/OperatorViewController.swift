//
//  OperatorViewController.swift
//  Orenda
//
//  Created by Arvind Mehta on 02/12/18.
//  Copyright © 2018 Arvind Mehta. All rights reserved.
//

import UIKit
import Kingfisher
import SwiftEventBus

class OperatorViewController: BaseViewController ,UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return operatorList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cellIdentifier = "OperatorTableViewCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! OperatorTableViewCell
        let rechargeobj = operatorList[indexPath.row]
       // displayImage(imageView: cell.opImage, path: "\(getUrl(path: "ImageBaseUrl"))\(rechargeobj.opratorImage)")
//        let imagePath = "\(getUrl(path: "ImageBaseUrl"))\(rechargeobj.opratorImage)"
//        Nuke.loadImage(
//            with: imagePath,
//            options: ImageLoadingOptions(
//
//                transition: .fadeIn(duration: 0.33)
//            ),
//            into: ImageView
//        )
        let url = URL(string: "\(getImage())\(rechargeobj.opratorImage)")
        cell.opImage.kf.setImage(with: url)
        cell.opName.text = operatorList[indexPath.row].operatorName
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(Constants.screenType == "DTHController"){
             SwiftEventBus.post(Constants.Operator_Event, sender: operatorList[indexPath.row])
             dismiss(animated: true, completion: nil)
        }else{
             SwiftEventBus.post(Constants.Operator_Event, sender: operatorList[indexPath.row])
          self.switchControllersToCircle(type: searchType)
        }
       
        
        
      
        
    }
    

    @IBOutlet weak var imgbackMenu: UIImageView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var tableview: UITableView!
    var operatorList = [RechargeModel]()
    var searchType = "1"
    var setclose = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        tableview.delegate = self
        tableview.dataSource = self
        getOperators()
        gesterTouch()
        setUpEvent()
    }
    
    func setUpEvent()  {
        SwiftEventBus.onMainThread(self, name: Constants.Close_OperatorListing) { result in
            self.setclose = "close"
           
        }
       
    }
    
    func gesterTouch(){
        let tapdonthaveCode = UITapGestureRecognizer(target: self, action: #selector(OperatorViewController.tapbackPress))
        imgbackMenu.isUserInteractionEnabled = true
        imgbackMenu.addGestureRecognizer(tapdonthaveCode)
    }
    
    //Action
    @objc func tapbackPress() {
        dismiss(animated: true, completion: nil)
    }
    
    func getOperators()
    {
        getOperatorList(view: self.view, type: searchType){responce in
            self.operatorList = responce
            self.tableview.reloadData()
            
        }
    }
    

    override func viewDidDisappear(_ animated: Bool) {
      Log(type: "viewDidDisappear", msg: "viewDidDisappear")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        Log(type: "viewWillAppear", msg: "viewWillAppear")
    }
    override func viewDidAppear(_ animated: Bool) {
        Log(type: "viewDidAppear", msg: "viewDidAppear")
        if(setclose == "close"){
            dismiss(animated: false, completion: nil)
        }
    }
   
}
