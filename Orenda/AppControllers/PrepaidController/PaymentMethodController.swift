//
//  PaymentMethodController.swift
//  Orenda
//
//  Created by Pixel on 02/01/19.
//  Copyright © 2019 Arvind Mehta. All rights reserved.
//

import UIKit

class PaymentMethodController: BaseViewController {
    var plan = ""
    var talkTime = ""
    var validity = ""
    var opoperator = ""
    var mobile = ""
    var amount = ""
    var type = ""
    var operator_id = ""
    var rupeesSign = "INR"
    var walletcheck = 0
     var payyoucheck = 0
    var isPaymentDone = false
    @IBAction func goToHome(_ sender: UIButton) {
    
    }
    @IBAction func crossicon(_ sender: UIButton) {
            self.switchControllersToNew(identifier: Constants.SWRevealViewController)
    }
    @IBAction func goTOHome(_ sender: UIButton) {
            self.switchControllersToNew(identifier: Constants.SWRevealViewController)
    }
    @IBAction func backThroughCross(_ sender: UIButton) {
         self.switchControllersToNew(identifier: Constants.SWRevealViewController)
    }
    @IBOutlet weak var cardViewBackground: UIView!
    
    @IBOutlet weak var cardViewCross: UIButton!
    @IBOutlet weak var cardViewNumber: UILabel!
    @IBOutlet weak var cardViewTranSiction: UILabel!
    @IBOutlet weak var cardViewDate: UILabel!
    
    @IBOutlet weak var cardViewAmount: UILabel!
    @IBOutlet weak var cardViewGoToHome: UILabel!
    @IBOutlet weak var card_view_popup: CardView!
    
    @IBOutlet weak var continuePayment: UIButton!
    @IBOutlet weak var payyoubutton: UIButton!
    @IBOutlet weak var walletbutton: UIButton!
    @IBOutlet weak var rechargeAmount: UILabel!
    
    @IBAction func payu_check(_ sender: UIButton) {
        payyoucheck += 1
        if(payyoucheck % 2 == 0){
            print(payyoucheck)
            print("even......")
            var yourImage: UIImage = UIImage(named: "uncheck")!
            payuwalletcheck.setImage(yourImage, for: UIControl.State.normal)
        }else{
            print(payyoucheck)
            print("uneven......")
            var yourImage: UIImage = UIImage(named: "check")!
            payuwalletcheck.setImage(yourImage, for: UIControl.State.normal)
        }
    }
    @IBOutlet weak var walletBalance: UILabel!
    
    @IBOutlet weak var AmountToPay: UILabel!
    
    @IBOutlet weak var checkboxwallet: UIButton!
    
    @IBOutlet weak var payuwalletcheck: UIButton!
    @IBAction func check_box_wallet_click(_ sender: UIButton) {
       walletcheck += 1
        if(walletcheck % 2 == 0){
            print(walletcheck)
            print("even......")
            var yourImage: UIImage = UIImage(named: "uncheck")!
            checkboxwallet.setImage(yourImage, for: UIControl.State.normal)
        }else{
               print(walletcheck)
             print("uneven......")
            var yourImage: UIImage = UIImage(named: "check")!
            checkboxwallet.setImage(yourImage, for: UIControl.State.normal)
        }
        
    }
    
    @IBOutlet weak var imagebackmenu: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(operator_id)
        card_view_popup.isHidden = true
        cardViewBackground.isHidden = true
        let walletBalan = getSharedPrefrance(key: Constants.USER_BALANCE)
        rechargeAmount.text = "\(rupeesSign) \(amount)"
        walletBalance.text = "\(rupeesSign) \(walletBalan)"
        AmountToPay.text = "\(rupeesSign) \(amount)"
           self.payyoubutton.setTitle("\(rupeesSign) \(amount)", for: .normal)
           self.walletbutton.setTitle("\(rupeesSign) \(walletBalan)", for: .normal)
        gesterTouch()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func ContinuePaymentTo(_ sender: UIButton) {
        let id_operator = operator_id
        let plan_talk_time = plan
        let plan_description = "Get 50 rupees extra in this recharge"
        let plan_validity = talkTime
        let plan_name = plan
        let type_rec = type
        let mobile_number = self.mobile
        let amount_rec = amount
        print(mobile_number)
        walletRecharge(id_operator:id_operator,plan_talk_time:plan_talk_time,plan_description:plan_description,plan_validity:plan_validity,plan_name:plan_name,type_rec:type_rec,mobile_number:mobile_number,amount_rec:amount_rec)
        
    }
    
    func walletRecharge(id_operator:String,plan_talk_time:String,plan_description:String,plan_validity:String,plan_name:String,type_rec:String,mobile_number:String,amount_rec:String)  {
        callWalletRecharge(view: self.view, id_operator:id_operator,plan_talk_time:plan_talk_time,plan_description:plan_description,plan_validity:plan_validity,plan_name:plan_name,type_rec:type_rec,mobile_number:mobile_number,amount_rec:amount_rec){response in
            Log(type: "PostpaidFetchBill", msg: response)
            if(response["error_code"].intValue == 400){
                showMessage(titleText: "Error", bodyText: response["message"].stringValue, msgtype: Constants.WARNING, duration: Constants.DURATION_MIDIUM)
            }else {
                self.isPaymentDone = true
               self.card_view_popup.isHidden = false
                self.cardViewBackground.isHidden = false
                showMessage(titleText: "Success", bodyText: response["message"].stringValue, msgtype: Constants.SUCCESS, duration: Constants.DURATION_MIDIUM)
                
                
                
            }
        }
    }
    
    func gesterTouch(){
        let tapdonthaveCode = UITapGestureRecognizer(target: self, action: #selector(PrepaidViewController.tapbackPress))
        imagebackmenu.isUserInteractionEnabled = true
        imagebackmenu.addGestureRecognizer(tapdonthaveCode)
    }
    
    //Action
    @objc func tapbackPress() {
        if(isPaymentDone == true){
            
        }else{
              dismiss(animated: true, completion: nil)
        }
    
    }

}
