//
//  WalletViewController.swift
//  Orenda
//
//  Created by Arvind Mehta on 01/12/18.
//  Copyright © 2018 Arvind Mehta. All rights reserved.
//

import UIKit

class WalletViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var historyTable: UITableView!
    @IBOutlet weak var saveCardTable: UITableView!
    @IBOutlet weak var btStartKyc: CardView!
    @IBOutlet weak var lbShowPrice: UILabel!
    @IBOutlet weak var btAddMonry: UIButton!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var imgBackMenu: UIImageView!
    @IBOutlet weak var btAddMoney: UIButton!
    //TODO Constraints
    
    @IBOutlet weak var kycViewHeight: NSLayoutConstraint!
    @IBOutlet weak var scrollViewHeight: NSLayoutConstraint!
    @IBOutlet weak var saveCardTableHeight: NSLayoutConstraint!
    @IBOutlet weak var historyTableheight: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btAddMoney.layer.cornerRadius = 20
        btAddMoney.clipsToBounds = true
        gesterTouch()
        autoCallApis()
        gesterTouch()
        gesterTouch()
        
    }
    
    func gesterTouch(){
        let tapdonthaveCode = UITapGestureRecognizer(target: self, action: #selector(WalletViewController.tapbackPress))
        imgBackMenu.isUserInteractionEnabled = true
        imgBackMenu.addGestureRecognizer(tapdonthaveCode)
    }
    
    //Action
    @objc func tapbackPress() {
        dismiss(animated: true, completion: nil)
    }
    
    func autoCallApis() {
        fetchWalletData()
    }
    
    @IBAction func onSeeAllClick(_ sender: Any) {
    }
    
    @IBAction func onAddMoneyClick(_ sender: Any) {
    }
    
    @IBAction func onAddNewCardClick(_ sender: Any) {
    }
    
    //Api Calling Methods
    
    func fetchWalletData(){
        getWalletData(view: self.view){ responce in
            
        }
    }
    
}
