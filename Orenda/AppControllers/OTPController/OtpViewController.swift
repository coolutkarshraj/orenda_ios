//
//  OtpViewController.swift
//  Orenda
//
//  Created by Arvind Mehta on 22/11/18.
//  Copyright © 2018 Arvind Mehta. All rights reserved.
//

import UIKit

class OtpViewController: BaseViewController {
    
    var seconds = 30
    @IBOutlet weak var lb_resendin_30: UILabel!
    @IBOutlet weak var tf_otpText: UITextField!
    var timer = Timer()
    var isTimerRunning = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

       runTimer()
    }
    
    func validateFields() -> Bool{
        var status = true
        if((tf_otpText.text?.isBlank)!){
            status = false
            showMessage(titleText: "Error", bodyText: "Please enter valid OTP", msgtype: Constants.ERROR, duration: Constants.DURATION_SHORT)
        }
        return status
    }
    
    func runTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(OtpViewController.updateTimer)), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer() {
        seconds -= 1
        lb_resendin_30.text = "Resend Otp in : \(seconds)"
        if(seconds == 0){
            timer.invalidate()
            resendOTP()
        }
    }
    

    @IBAction func onContinueClick(_ sender: Any) {
        if(validateFields()){
            
            otpVerification(view: self.view, mobileNumber: getSharedPrefrance(key: Constants.USER_MOBILE), otp: tf_otpText.text!, deviceInfo: "IOS info"){responce in
               
                    self.timer.invalidate()
                
                self.switchControllersToNew(identifier: Constants.SWRevealViewController)
            }
        }
    }
    
    func resendOTP(){
        otpResend(view: self.view, mobileNumber: getSharedPrefrance(key: Constants.USER_MOBILE), otp: "", deviceInfo: ""){responce in
            Log(type: "Responce", msg: responce)
        }
    }
  

}
