//
//  SearchResult.swift
//  ConnectAdlinks
//
//  Created by Arvind Mehta on 27/11/18.
//  Copyright © 2018 Pixel. All rights reserved.
//

import Foundation
import SwiftyJSON
class SearchReselt{
    
    var id:String? = ""
    var city:String? = ""
    var name:String? = ""
    var airport_code:String? = ""
    var flight_type:String? = ""
    var country:String? = ""
    init(json:JSON) {
        
        self.id = json["id"].stringValue
         self.airport_code = json["airport_code"].stringValue
        self.city = json["city"].stringValue
         self.flight_type = json["flight_type"].stringValue
        self.country = json["country"].stringValue
        
    }
    
}
