//
//  DataSvcController.swift
//  Kikkle
//
//  Created by Utkarsh Raj on 06/09/18.
//  Copyright © 2019 Utkarsh Raj. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

 enum MyException: Error {
    case runtimeError(String,Int)
}

func authentication(view:UIView ,email:String,password:String, completion: @escaping (JSON) -> ()) {
    let param = [ ApiParam.email:email,
                  ApiParam.password:password,
                  ApiParam.fcm_token:"IOSToken",
                  ApiParam.device_type:"IOS",
                  ApiParam.device_id:"xyz",
                  ApiParam.device_info:"IOSxyz"]
    
    executePOST(view:view, path: "Login", parameter: param){responce in
        Log(type: "Register responce", msg: responce)
        
        if(responce["success"].bool == true){
            showMessage(titleText: "Success", bodyText: responce["message"].stringValue, msgtype: Constants.SUCCESS, duration: Constants.DURATION_SHORT)
            setRegisterData(data: responce)
            completion(responce)
        }else{
            showMessage(titleText: "Failed", bodyText: responce["message"].stringValue, msgtype: Constants.WARNING, duration: Constants.DURATION_SHORT)
        }
    }
    
}
func register(view:UIView ,email:String,password:String,mobileNumber:String, completion: @escaping (JSON) -> ()) {
    let param = [ ApiParam.email:email,
                  ApiParam.mobile_number:mobileNumber,
                  ApiParam.password:password,
                  ApiParam.fcm_token:"IOSToken"]
    
    executePOST(view:view, path: "SignUP", parameter: param){responce in
        Log(type: "Register responce", msg: responce)
        
        if(responce["success"].bool == true){
            showMessage(titleText: "Success", bodyText: responce["message"].stringValue, msgtype: Constants.SUCCESS, duration: Constants.DURATION_SHORT)
            setRegisterData(data: responce)
            completion(responce)
        }else{
            showMessage(titleText: "Failed", bodyText: responce["message"].stringValue, msgtype: Constants.WARNING, duration: Constants.DURATION_SHORT)
        }
    }
   
}


func otpVerification(view:UIView ,mobileNumber:String,otp:String, deviceInfo:String,completion: @escaping (JSON) -> ()){
    let param = [ApiParam.mobile_number:mobileNumber,
                 ApiParam.otp:otp,
                 ApiParam.device_type:"IOS",
                 ApiParam.device_id:deviceInfo,
                 ApiParam.device_info:"123",
                 ApiParam.fcm_token:"xyz"]
    
    executePOST(view: view, path: "Login", parameter: param){responce in
        Log(type: "Register responce", msg: responce)
        if(responce["success"].bool == true){
            showMessage(titleText: "Success", bodyText: responce["message"].stringValue, msgtype: Constants.SUCCESS, duration: Constants.DURATION_SHORT)
            setRegisterData(data: responce)
            completion(responce)
        }else{
            showMessage(titleText: "Failed", bodyText: responce["message"].stringValue, msgtype: Constants.WARNING, duration: Constants.DURATION_SHORT)
        }
    }
    
}

func otpResend(view:UIView ,mobileNumber:String,otp:String, deviceInfo:String,completion: @escaping (JSON) -> ()){
    let postData = NSMutableData(data: "mobile_number=\(mobileNumber)".data(using: String.Encoding.utf8)!)
    postData.append("&undefined=undefined".data(using: String.Encoding.utf8)!)
    
    executePUT(view: view, path: "OTP_resend", parameter: postData){responce in
        Log(type: "Register responce", msg: responce)
        if(responce["success"].bool == true){
            showMessage(titleText: "Success", bodyText: responce["message"].stringValue, msgtype: Constants.SUCCESS, duration: Constants.DURATION_SHORT)
            setRegisterData(data: responce)
        }else{
            showMessage(titleText: "Failed", bodyText: responce["message"].stringValue, msgtype: Constants.WARNING, duration: Constants.DURATION_SHORT)
        }
    }
    
}



func resetPassword(view:UIView ,password:String,cn_password:String, recovery_token:String,completion: @escaping (JSON) -> ()){
    let postData = NSMutableData(data: "\(ApiParam.password)=\(password)".data(using: String.Encoding.utf8)!)
    postData.append("&\(ApiParam.cn_password)=\(cn_password)".data(using: String.Encoding.utf8)!)
    postData.append("&\(ApiParam.device_type)=\(recovery_token)".data(using: String.Encoding.utf8)!)
    executePUT(view: view, path: "ResetPassword", parameter: postData){responce in
        Log(type: "Register responce", msg: responce)
        if(responce["success"].bool == true){
            showMessage(titleText: "Success", bodyText: responce["message"].stringValue, msgtype: Constants.SUCCESS, duration: Constants.DURATION_SHORT)
            setRegisterData(data: responce)
        }else{
            showMessage(titleText: "Failed", bodyText: responce["message"].stringValue, msgtype: Constants.WARNING, duration: Constants.DURATION_SHORT)
        }
    }
    
}


func forgotPassword(view:UIView ,email:String,completion: @escaping (JSON) -> ()){
    let postData = NSMutableData(data: "\(ApiParam.email)=\(email)".data(using: String.Encoding.utf8)!)
   
    executePUT(view: view, path: "ForgotPassword", parameter: postData){responce in
        Log(type: "Register responce", msg: responce)
        if(responce["success"].bool == true){
            showMessage(titleText: "Success", bodyText: responce["message"].stringValue, msgtype: Constants.SUCCESS, duration: Constants.DURATION_SHORT)
            
        }else{
            showMessage(titleText: "Failed", bodyText: responce["message"].stringValue, msgtype: Constants.WARNING, duration: Constants.DURATION_SHORT)
        }
    }
    
}

// Wallet
func getWalletData(view:UIView ,completion: @escaping (JSON) -> ()){
    let access_token = getSharedPrefrance(key: Constants.USER_TOKEN)
    let postData = NSMutableData(data: "\(ApiParam.access_token)=\(access_token)".data(using: String.Encoding.utf8)!)
    executeGET(view: view, path: "Wallet", parameter: postData){ responce in
        Log(type: "Wallet", msg: responce)
    }
  
}
// Operator List
func getOperatorList(view:UIView ,type:String,completion: @escaping ([RechargeModel]) -> ()){
    
    let param = [String:String]()
    let path = getUrl(path: "Operator")+"?type=\(type)"
    Log(type: "path", msg: path)
    executeCustomGet(view: view, path: path, param: param){responce in
        Log(type: "Operator", msg: responce)
        if(responce["success"].boolValue == true){
            var rechargemodelarray = [RechargeModel]()
            for item in responce["data"].arrayValue{
                rechargemodelarray.append(RechargeModel(json: item))
            }
            completion(rechargemodelarray)
        }else{
            showMessage(titleText: "Failed", bodyText: responce["message"].stringValue, msgtype: Constants.WARNING, duration: Constants.DURATION_SHORT)
        }
    }
    
}

//Full Talk Time

func getFulltalkTime(view:UIView ,count:Int,operatorText:String,circle:String,type:String,completion: @escaping ([PlanModel]) -> ()){
    
    let param = [String:String]()
    let opEncoded = operatorText.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
    let circleEncode = circle.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
    let path = "\(getUrl())\(ApiParam.recharge_plan)?operator=\(opEncoded!)&circle=\(circleEncode!)&recharge_type=\(type)&page=\(count)"
    Log(type: "path", msg: path)
    executeCustomGet(view: view, path: path, param: param){responce in
        Log(type: "Operator", msg: responce)
        if(responce["success"].boolValue == true){
            var planAray = [PlanModel]()
            for item in responce["data"]["data"].arrayValue{
                planAray.append(PlanModel(json: item.dictionaryObject!)!)
            }
            completion(planAray)
        }else{
            showMessage(titleText: "Failed", bodyText: responce["message"].stringValue, msgtype: Constants.WARNING, duration: Constants.DURATION_SHORT)
        }
    }
    
}

func getDthPlan(view:UIView ,count:Int,operatorText:String,circle:String,type:String,completion: @escaping ([PlanModel]) -> ()){
    
    let param = [String:String]()
    let opEncoded = operatorText.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
    let circleEncode = circle.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
    let path = "\(getUrl())\(ApiParam.dth_plan)?operator=\(opEncoded!)&recharge_type=\(type)&page=\(count)"
    Log(type: "path", msg: path)
    executeCustomGet(view: view, path: path, param: param){responce in
        Log(type: "Operator", msg: responce)
        if(responce["success"].boolValue == true){
            var planAray = [PlanModel]()
            for item in responce["data"]["data"].arrayValue{
                planAray.append(PlanModel(json: item.dictionaryObject!)!)
            }
            completion(planAray)
        }else{
            showMessage(titleText: "Failed", bodyText: responce["message"].stringValue, msgtype: Constants.WARNING, duration: Constants.DURATION_SHORT)
        }
    }
    
}


// Circle List
func getcircleList(view:UIView ,type:String,completion: @escaping ([RechargeModel]) -> ()){
    
    var append = ""
    let param = [String:String]()
   
    if(type == "1" || type ==  "2"){
        append = ApiParam.op_circle
    }else if(type == "6" || type == "7"){
         append = ApiParam.datacard_circle
    }
    let path = getUrl()+"\(append)?type=\(type)"
    Log(type: "path", msg: path)
    executeCustomGet(view: view, path: path, param: param){responce in
        Log(type: "circle", msg: responce)
        if(responce["success"].boolValue == true){
            var rechargemodelarray = [RechargeModel]()
            for item in responce["data"].arrayValue{
                rechargemodelarray.append(RechargeModel(json: item))
            }
            completion(rechargemodelarray)
        }else{
            showMessage(titleText: "Failed", bodyText: responce["message"].stringValue, msgtype: Constants.WARNING, duration: Constants.DURATION_SHORT)
        }
    }
    
}
// Number Look Up List
func getNumberLookUp(view:UIView ,number:String,completion: @escaping ([String:String]) -> ()){
    
  
    let param = [String:String]()
  
    let path = getUrl()+"\(ApiParam.mobile_lookup)?mobile_number=\(number)"
    Log(type: "path", msg: path)
    executeCustomGet(view: view, path: path, param: param){responce in
        Log(type: "circle", msg: responce)
        if(responce["success"].boolValue == true){
            var returnArray = [String:String]()
            returnArray = ["operator":responce["data"]["operator"].stringValue,
            "circle":responce["data"]["circle"].stringValue]
          
            completion(returnArray)
        }else{
            showMessage(titleText: "Failed", bodyText: responce["message"].stringValue, msgtype: Constants.WARNING, duration: Constants.DURATION_SHORT)
        }
    }
    
}


func callGetBillData(view:UIView,mobile:String,amount:String,optId:String,rectype:String, account:String,bill_fetch:String,completion:@escaping (JSON) -> ()){
    let param = [ ApiParam.mobile:mobile,
                  ApiParam.amount:amount,
                  ApiParam.optId:optId,
                  ApiParam.rectype:rectype,
                  ApiParam.account:account,
                  ApiParam.bill_fetch:bill_fetch,
                  ]
    
    print(param)
    let urlpath = getUrl()+"recharge"
    print(urlpath)
    
    executePOSTWithHeader(view: view, path:urlpath, parameter: param){responce in
        Log(type: "Register responce", msg: responce)
        print("response.....")
        if(responce["success"].bool == true){
            showMessage(titleText: "Success", bodyText: responce["message"].stringValue, msgtype: Constants.SUCCESS, duration: Constants.DURATION_SHORT)
            completion(responce)
        }else{
            print(responce)
            let bill = responce["bill"] as? [[String: Any]] ?? []
            print(bill)
            showMessage(titleText: "Failed", bodyText: responce["message"].stringValue, msgtype: Constants.WARNING, duration: Constants.DURATION_SHORT)
        }
    }
    
}

func callWalletRecharge(view:UIView,id_operator:String,plan_talk_time:String,plan_description:String,plan_validity:String,plan_name:String,type_rec:String,mobile_number:String,amount_rec:String,completion:@escaping (JSON) -> ()){
    let param1 = ["operator_id":id_operator,
                 "plan_description": "Talktime of Rs.100 for Rs.100 recharge by Jio",
                 "plan_talktime":amount_rec,
                 "plan_validity":plan_validity,
                 "plan_name":plan_name,
                 "type":type_rec,
                 "mobile_number":mobile_number,
                 "amount":amount_rec,]
    
    print(param1)
    let urlpath = getUrl()+"recharge"
    print(urlpath)
    
    executePOSTWithHeader(view: view, path:urlpath ,parameter: param1){responce in
        Log(type: "Register responce", msg: responce)
        print("response.....")
        if(responce["success"].bool == true){
            showMessage(titleText: "Success", bodyText: responce["message"].stringValue, msgtype: Constants.SUCCESS, duration: Constants.DURATION_SHORT)
            completion(responce)
        }else{
            print(responce)
            let bill = responce["bill"] as? [[String: Any]] ?? []
            print(bill)
            showMessage(titleText: "Failed", bodyText: responce["message"].stringValue, msgtype: Constants.WARNING, duration: Constants.DURATION_SHORT)
        }
    }
    
}

func dopaymentFromWallet(view:UIView ,type:String,completion: @escaping ([RechargeModel]) -> ()){
    
    var append = ""
    let param = [String:String]()
    
    if(type == "1" || type ==  "2"){
        append = ApiParam.op_circle
    }else if(type == "6" || type == "7"){
        append = ApiParam.datacard_circle
    }
    let path = getUrl()+"\(append)?type=\(type)"
    Log(type: "path", msg: path)
    executeCustomGet(view: view, path: path, param: param){responce in
        Log(type: "circle", msg: responce)
        if(responce["success"].boolValue == true){
            var rechargemodelarray = [RechargeModel]()
            for item in responce["data"].arrayValue{
                rechargemodelarray.append(RechargeModel(json: item))
            }
            completion(rechargemodelarray)
        }else{
            showMessage(titleText: "Failed", bodyText: responce["message"].stringValue, msgtype: Constants.WARNING, duration: Constants.DURATION_SHORT)
        }
    }
    
}



//Helper Functions

func setRegisterData(data:JSON){
    if(data["data"]["mobile_number"].exists()){
         savesharedprefrence(key: Constants.USER_MOBILE, value: data["data"]["mobile_number"].stringValue)
    }
    if(data["data"]["otp"].exists()){
        savesharedprefrence(key: Constants.USER_OTP, value: data["data"]["otp"].stringValue)
    }
    if(data["data"]["email"].exists()){
        savesharedprefrence(key: Constants.USER_EMAIL, value: data["data"]["email"].stringValue)
    }
    if(data["data"]["fcm_token"].exists()){
        savesharedprefrence(key: Constants.USER_DEVICE_NOTIFICATION_TOKEN, value: data["data"]["fcm_token"].stringValue)
    }
    if(data["data"]["full_name"].exists()){
        savesharedprefrence(key: Constants.USER_FULL_NAME, value: data["data"]["full_name"].stringValue)
    }
    if(data["data"]["device_info"].exists()){
        savesharedprefrence(key: Constants.USER_DEVICE_INFO, value: data["data"]["device_info"].stringValue)
    }
    if(data["data"]["login_status"].exists()){
        savesharedprefrence(key: Constants.USER_LOGIN_STATUS, value: data["data"]["login_status"].stringValue)
    }
    if(data["data"]["user_id"].exists()){
        savesharedprefrence(key: Constants.USER_ID, value: data["data"]["user_id"].stringValue)
    }
    if(data["data"]["balance"].exists()){
        savesharedprefrence(key: Constants.USER_BALANCE, value: data["data"]["balance"].stringValue)
    }
    if(data["data"]["balance"].exists()){
        savesharedprefrence(key: Constants.USER_BALANCE, value: data["data"]["balance"].stringValue)
    }
    if(data["data"]["access_token"].exists()){
        savesharedprefrence(key: Constants.USER_TOKEN, value: data["data"]["access_token"].stringValue)
    }
    
    
    
   
}



