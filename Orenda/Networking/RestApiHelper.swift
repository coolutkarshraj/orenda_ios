//
//  Networking.swift
//  Imobpay
//
//  Created by Arvind Mehta on 30/05/17.
//  Copyright © 2017 Arvind. All rights reserved.
//

import Foundation
import Alamofire
import UIKit
import SwiftyJSON





func executePOST(view:UIView,path:String, parameter: [String: String] , completion: @escaping (JSON) -> ()) {
    
    if ARSLineProgress.shown { return }
    ARSLineProgress.show()
    
   
   
    var headers =  [String: String]()
   
        headers = ["cache-control": "no-cache"]
        
   
   // let api = Alamofire.SessionManager(configuration: configuration)
    print(getUrl(path: path))
    Alamofire.request(getUrl(path: path) ,method: .post, parameters: parameter, encoding: URLEncoding.httpBody,headers: headers).validate(statusCode: 200..<500).responseJSON {response in
        
        switch response.result {
        case .success:
             ARSLineProgress.showSuccess()
            do {
                let jsonData = try JSON(data: response.data!)
                completion(jsonData)
                
            }catch{
            }
            
        case .failure:
             ARSLineProgress.showFail()
            do {
                try completion(JSON(data: NSData() as Data))
            }catch{
                
            }
        }
      
    }
}


func executePOSTWithHeader(view:UIView,path:String, parameter: [String: String] , completion: @escaping (JSON) -> ()) {
    
    if ARSLineProgress.shown { return }
    ARSLineProgress.show()
    
    
    
    var headers =  [String: String]()
    
    headers = ["Accept": "application/json"]
    headers["access_token"] = getSharedPrefrance(key: Constants.USER_TOKEN)
    
    // let api = Alamofire.SessionManager(configuration: configuration)
    print(getUrl(path: path))
    Alamofire.request("https://once.pixelsoftwares.com/orenda/api/recharge" ,method: .post, parameters: parameter, encoding: URLEncoding.httpBody,headers: headers).validate(statusCode: 200..<500).responseJSON {response in
        
        switch response.result {
        case .success:
            ARSLineProgress.showSuccess()
            do {
                let jsonData = try JSON(data: response.data!)
                completion(jsonData)
            }catch{
                print("error")
            }
            
        case .failure:
            ARSLineProgress.showFail()
            do {
                try completion(JSON(data: NSData() as Data))
            }catch{
                print("error")
            }
        }
        
    }
}


func executePOSTConnectAdd(view:UIView,path:String, parameter: [String: String] , completion: @escaping (JSON) -> ()) {
    
//    if ARSLineProgress.shown { return }
//    ARSLineProgress.show()
    
    
    
    var headers =  [String: String]()
    
    headers = ["sicherheit":"88ef1e6afb78cd4f093acb2e27dd9627"]
    
    
    // let api = Alamofire.SessionManager(configuration: configuration)
    print(path)
    Alamofire.request(path,method: .post, parameters: parameter, encoding: URLEncoding.httpBody,headers: headers).validate(statusCode: 200..<500).responseJSON {response in
        
        switch response.result {
        case .success:
         //   ARSLineProgress.showSuccess()
            do {
                let jsonData = try JSON(data: response.data!)
                completion(jsonData)
                
            }catch{
            }
            
        case .failure:
           // ARSLineProgress.showFail()
            do {
                try completion(JSON(data: NSData() as Data))
            }catch{
                
            }
        }
        
    }
}


func executePUT(view:UIView,path:String, parameter: NSMutableData , completion: @escaping (JSON) -> ()) {
    if ARSLineProgress.shown { return }
    ARSLineProgress.show()
    
    let headers = [
        "Content-Type": "application/x-www-form-urlencoded",
        "cache-control": "no-cache"
    ]
    
   
     print(getUrl(path: path))
    let request = NSMutableURLRequest(url: NSURL(string: getUrl(path: path))! as URL,
                                      cachePolicy: .useProtocolCachePolicy,
                                      timeoutInterval: 10.0)
    request.httpMethod = "PUT"
    request.allHTTPHeaderFields = headers
    request.httpBody = parameter as Data
    
    let session = URLSession.shared
    let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
        
        if (error != nil) {
        DispatchQueue.main.async {
            ARSLineProgress.showFail()
            do {
                try completion(JSON(data: NSData() as Data))
            }catch{
                
            }
            }
        } else {
            DispatchQueue.main.async {
                do{
                    ARSLineProgress.showSuccess()
                    let json = try JSON(data: data!)
                    completion(json)
                    
                }catch{
                    
                }
            }
           
           
        }
    })
    
    dataTask.resume()
    
   
}



func executeGET(view:UIView,path:String , completion: @escaping (JSON) -> ()) {
    if ARSLineProgress.shown { return }
    ARSLineProgress.show()
    
    let   headers = ["cache-control": "no-cache"]
   
    Alamofire.request(getUrl(path: path),method: .get , encoding: URLEncoding.httpBody,headers: headers).validate(statusCode: 200..<500).responseJSON { response in
        view.hideToastActivity()
        switch response.result {
        case .success:
           ARSLineProgress.showFail()
            do {
                let jsonData = try JSON(data: response.data!)
                completion(jsonData)
            }catch{
                
            }
            
        case .failure:
            ARSLineProgress.showFail()
            do {
                try completion(JSON(data: NSData() as Data))
            }catch{
                
            }
        }
    }
    
}
func executeGET(view:UIView,path:String , parameter: NSMutableData  ,completion: @escaping (JSON) -> ()) {
    if ARSLineProgress.shown { return }
    ARSLineProgress.show()
    
    let headers = [
        "Content-Type": "application/x-www-form-urlencoded",
        "cache-control": "no-cache"
    ]
    
    
    print(getUrl(path: path))
    let request = NSMutableURLRequest(url: NSURL(string: getUrl(path: path))! as URL,
                                      cachePolicy: .useProtocolCachePolicy,
                                      timeoutInterval: 10.0)
    request.httpMethod = "GET"
    request.allHTTPHeaderFields = headers
    request.httpBody = parameter as Data
    
    let session = URLSession.shared
    let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
        
        if (error != nil) {
            DispatchQueue.main.async {
                ARSLineProgress.showFail()
                do {
                    try completion(JSON(data: NSData() as Data))
                }catch{
                    
                }
            }
        } else {
            DispatchQueue.main.async {
                do{
                    ARSLineProgress.showSuccess()
                    let json = try JSON(data: data!)
                    completion(json)
                    
                }catch{
                    
                }
            }
            
            
        }
    })
    
    dataTask.resume()
    
    
}

func executeCustomGet(view:UIView,path:String ,param:[String:String], completion: @escaping (JSON) -> ()) {
    if ARSLineProgress.shown { return }
    ARSLineProgress.ars_showOnView(view)
    
    let   headers = ["Accept": "application/json"]
    Log(type: "Custompath", msg: path)
    if(!param.isEmpty){
        Alamofire.request(path,method: .get , parameters: param,encoding: URLEncoding.httpBody,headers: headers).validate(statusCode: 200..<500).responseJSON { response in
            
            switch response.result {
            case .success:
                ARSLineProgress.showSuccess()
                do {
                    let jsonData = try JSON(data: response.data!)
                    completion(jsonData)
                }catch{
                    
                }
                
            case .failure:
                ARSLineProgress.showFail()
                do {
                    try completion(JSON(data: NSData() as Data))
                }catch{
                    
                }
            }
        }
    }else{
        
        let   headers = ["Accept": "application/json"]
        
        Alamofire.request(path,method: .get , encoding: URLEncoding.httpBody,headers: headers).validate(statusCode: 200..<500).responseJSON { response in
            
            switch response.result {
            case .success:
                ARSLineProgress.showSuccess()
                do {
                    let jsonData = try JSON(data: response.data!)
                    completion(jsonData)
                }catch{
                    
                }
                
            case .failure:
                ARSLineProgress.showFail()
                do {
                    try completion(JSON(data: NSData() as Data))
                }catch{
                    
                }
            }
        }
    }
    
}






    func executeMultiPartForm(view:UIView,path:String, parameter:Parameters ,imageData: [Data], completion: @escaping (JSON) -> ()){
        
        let url = "\(getApiDic(key: "BaseURL"))\(getApiDic(key: path))"
        
        var headers =  [String: String]()
        if(!getSharedPrefrance(key: Constants.USER_TOKEN).isEmpty)
        {
            headers = ["token": getApiDic(key: "AUTH_TOKEN"),
                       "token_id": getSharedPrefrance(key: Constants.USER_TOKEN)]
           //
            print(headers)
            
        }else
        {
            headers = ["Token": getApiDic(key: "AUTH_TOKEN")]
        }
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameter {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
            for dat in imageData{
                multipartFormData.append(dat, withName: "image_id", fileName: "image.png", mimeType: "image/png")
            }
          
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print("Succesfully uploaded")
                    if response.error != nil{
                        do {
                            try completion(JSON(data: NSData() as Data))
                        }catch{
                        }
                        return
                        
                    }
                    do {
                        let jsonData = try JSON(data: response.data!)
                        if(jsonData["status"].stringValue == "703"){
                            
                            //let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            
                           // appDelegate.expiredToken()
                            //view.window!.rootViewController?.dismiss(animated: false, completion: nil)
                        }else{
                            completion(jsonData)
                        }
                        
                    }catch{
                    }
                  
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
               
            }
        }
    }

func displayImage(imageView:UIImageView,path:String){
    imageView.contentMode = UIView.ContentMode.scaleAspectFit
  
    Alamofire.request(path).responseData { response in
        
        if let image = response.data {
            print("image downloaded: \(image)")
            imageView.image = UIImage(data: image)
        }
    }
    
    
}

func downloadImage(imageView:UIImageView,path:String, completion: @escaping (UIImage) -> ()){
    imageView.contentMode = UIView.ContentMode.scaleAspectFit
   
    Alamofire.request(path).responseData { response in
        
        if let image = response.data {
            print("image downloaded: \(image)")
            imageView.image = UIImage(data: image)
            completion(UIImage(data: image)!)
        }
    }
    
    
}




  func getApiDic(key:String) -> String {
        let levelBlocks = NSDictionary(contentsOfFile: Bundle.main.path(forResource: "SecureConstants", ofType: "plist")!)
        let keyresponce: AnyObject = levelBlocks!.object(forKey: key) as AnyObject
        Log(type: "oBject From Plist", msg: "\(keyresponce)")
        return "\(keyresponce)"
    }

func getRendomStrin() -> String  {
    let dateFormatter : DateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    let date = Date()
   // let dateString = dateFormatter.string(from: date)
    
    let interval = date.timeIntervalSince1970
    
    return "\(interval)"
}

func getUrl(path:String) -> String {
    var url  = ""
    #if DEBUG
    url =  "\(getApiDic(key: "Stageurl"))\(getApiDic(key: path))"
    #else
    url =  "\(getApiDic(key: "Stageurl"))\(getApiDic(key: path))"
    #endif
    
    return url
}

func getUrl() -> String {
    var url  = ""
    #if DEBUG
    url =  "\(getApiDic(key: "Stageurl"))"
    #else
    url =  "\(getApiDic(key: "Stageurl"))"
    #endif
    
    return url
}

func getImage() -> String {
    var url  = ""
    #if DEBUG
    url =  "\(getApiDic(key: "ImageBaseUrl"))"
    #else
    url =  "\(getApiDic(key: "ImageBaseUrl"))"
    #endif
    
    return url
}











