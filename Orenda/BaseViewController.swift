//
//  BaseViewController.swift
//  Orenda
//
//  Created by Arvind Mehta on 20/11/18.
//  Copyright © 2018 Arvind Mehta. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    /* Switching Controllers Process*/
    
    func switchControllersToNew(identifier:String){
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let viewController = storyboard.instantiateViewController(withIdentifier: localizedString(forKey: identifier))
        let modalStyle: UIModalTransitionStyle = UIModalTransitionStyle.crossDissolve
        viewController.modalTransitionStyle = modalStyle
        self.present(viewController, animated: true, completion: nil)
        
    }
    
    func switchControllersToOperator(type:String){
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let viewController = storyboard.instantiateViewController(withIdentifier: localizedString(forKey: Constants.OPERATOR_SELECT)) as! OperatorViewController
        viewController.searchType = type
        let modalStyle: UIModalTransitionStyle = UIModalTransitionStyle.crossDissolve
        viewController.modalTransitionStyle = modalStyle
        self.present(viewController, animated: true, completion: nil)
        
    }
    func switchControllersToCircle(type:String){
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let viewController = storyboard.instantiateViewController(withIdentifier: localizedString(forKey: Constants.CIRCLE_CONTROLLER)) as! CircleViewController
        viewController.rechargeType = type
        let modalStyle: UIModalTransitionStyle = UIModalTransitionStyle.crossDissolve
        viewController.modalTransitionStyle = modalStyle
        self.present(viewController, animated: true, completion: nil)
        
    }
    func switchControllersToSeePlans(type:String,circle:String,opoperator:String){
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let viewController = storyboard.instantiateViewController(withIdentifier: localizedString(forKey: Constants.SEEPLAN_CONTROLLER)) as! SeePlanViewController
        viewController.rechargeType = type
        viewController.selectedCircle = circle
        viewController.selectedOperator = opoperator
        let modalStyle: UIModalTransitionStyle = UIModalTransitionStyle.crossDissolve
        viewController.modalTransitionStyle = modalStyle
        self.present(viewController, animated: true, completion: nil)
        
    }
   
    func switchControllersToSeePlansData(type:String,circle:String,opoperator:String){
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let viewController = storyboard.instantiateViewController(withIdentifier: localizedString(forKey: Constants.DataCardPlanViewController)) as! DataCardPlanViewController
        viewController.rechargeType = type
        viewController.selectedCircle = circle
        viewController.selectedOperator = opoperator
        let modalStyle: UIModalTransitionStyle = UIModalTransitionStyle.crossDissolve
        viewController.modalTransitionStyle = modalStyle
        self.present(viewController, animated: true, completion: nil)
        
    }
    func switchControllersToSeePlansDTH(type:String,circle:String,opoperator:String){
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let viewController = storyboard.instantiateViewController(withIdentifier: localizedString(forKey: Constants.DTHPlanViewController)) as! DTHPlanViewController
        viewController.rechargeType = type
        viewController.selectedCircle = circle
        viewController.selectedOperator = opoperator
        let modalStyle: UIModalTransitionStyle = UIModalTransitionStyle.crossDissolve
        viewController.modalTransitionStyle = modalStyle
        self.present(viewController, animated: true, completion: nil)
        
    }
    
    func switchControllersToRechargeConfirm(tfOfSisiter:String,number:String,opId:String,rechtype:String,circle:String,selectedplan:String,selectedValidity:String,rechargeType:String){
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let viewController = storyboard.instantiateViewController(withIdentifier: localizedString(forKey: Constants.CONFIRM_RECHARGE)) as! ConfirmRechargeViewController
        
        viewController.amount = tfOfSisiter
        viewController.opoperator = circle
        viewController.operator_id = opId
        viewController.mobile = number
        viewController.type = rechargeType
        viewController.plan = selectedplan
        viewController.talkTime = selectedValidity
        let modalStyle: UIModalTransitionStyle = UIModalTransitionStyle.crossDissolve
        viewController.modalTransitionStyle = modalStyle
        self.present(viewController, animated: true, completion: nil)
        
    }
    
    func switchControllersToRechargeConfirmWithoutPlan(tfOfSisiter:String,number:String,opId:String,rechtype:String,circle:String,selectedplan:String,selectedValidity:String,rechargeType:String){
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let viewController = storyboard.instantiateViewController(withIdentifier: localizedString(forKey: Constants.CONFIRM_RECHARGE_Without_Plan)) as! ConfirmRechargeWithoutPlanViewController
        viewController.amount = tfOfSisiter
        viewController.opoperator = circle
        viewController.operator_id = opId
        viewController.mobile = number
        viewController.type = rechargeType
        viewController.plan = selectedplan
        viewController.talkTime = selectedValidity
        let modalStyle: UIModalTransitionStyle = UIModalTransitionStyle.crossDissolve
        viewController.modalTransitionStyle = modalStyle
        self.present(viewController, animated: true, completion: nil)
        
    }
    
    func switchLoginControllersfromRechargeConfirm(tfOfSisiter:String,number:String,opId:String,rechtype:String,circle:String,selectedplan:String,selectedValidity:String,rechargeType:String){
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let viewController = storyboard.instantiateViewController(withIdentifier: localizedString(forKey: Constants.LOGIN)) as! LoginViewController
        viewController.amount = tfOfSisiter
        viewController.opoperator = circle
        viewController.operator_id = opId
        viewController.mobile = number
        viewController.type = rechargeType
        viewController.plan = selectedplan
        viewController.talkTime = selectedValidity
        let modalStyle: UIModalTransitionStyle = UIModalTransitionStyle.crossDissolve
        viewController.modalTransitionStyle = modalStyle
        self.present(viewController, animated: true, completion: nil)
        
    }
    
    func
 switchpaymentConfirmation(tfOfSisiter:String,number:String,opId:String,rechtype:String,circle:String,selectedplan:String,selectedValidity:String,rechargeType:String){
    let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
    let viewController = storyboard.instantiateViewController(withIdentifier: localizedString(forKey: Constants.CONFIRM_RECHARGE_Payment)) as! PaymentMethodController
    viewController.amount = tfOfSisiter
    viewController.opoperator = circle
    viewController.operator_id = opId
    viewController.mobile = number
    viewController.type = rechargeType
    viewController.plan = selectedplan
    viewController.talkTime = selectedValidity
    let modalStyle: UIModalTransitionStyle = UIModalTransitionStyle.crossDissolve
    viewController.modalTransitionStyle = modalStyle
    self.present(viewController, animated: true, completion: nil)
    
    }
    
    
    
    /* ---- Localization Process Start -----*/
    
    func localizedString(forKey key: String) -> String
    {
        var result = Bundle.main.localizedString(forKey: key, value: nil, table: nil)
        
        if result == key
        {
            result = Bundle.main.localizedString(forKey: key, value: nil, table: "BaseConstant")
        }
        
        return result
    }
    /* ---- End Localization Process ------*/
    
}
